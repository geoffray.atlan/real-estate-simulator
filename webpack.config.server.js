const webpack = require("webpack");
const path = require("path");
const nodeExternals = require("webpack-node-externals");

module.exports = {
  entry: {
    server: "./src/server/index.ts",
  },
  mode: "production",
  optimization: {
    minimize: false,
  },
  target: "node",
  devtool: "source-map",

  externals: [nodeExternals({})],

  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: "ts-loader",
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  output: {
    path: path.join(__dirname, "build"),
  },
};

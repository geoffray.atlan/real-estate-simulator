### Production Dependencies

| **Dependency**     | **Use**                                              |
| ------------------ | ---------------------------------------------------- |
| axios              | HTTP Requests                                        |
| express            | Node.js web application framework                    |
| i18next            | Translations                                         |
| material-ui        | Deign Library                                        |
| mongodb            | Databsae                                             |
| nodemon            | reload automatically server                          |
| prop-types         | Declare types for props passed into React components |
| react              | React library                                        |
| react-dom          | React library for DOM rendering                      |
| react-redux        | Connects React components to Redux                   |
| react-router-dom   | React library for routing                            |
| redux              | Library for unidirectional data flows                |
| redux-thunk        | Async redux library                                  |
| redux-thunk        | Async redux library                                  |
| shepherd           | Tour Guide App                                       |
| styled-components  | CSS-in-JS libraries                                  |


### Development Dependencies

| **Dependency**                  | **Use**                                                          |
| ------------------------------- | ---------------------------------------------------------------- |
| @babel/core                     | Transpiles modern JavaScript so it runs cross-browser            |
| @babel/node                     | Babel presets with Node.js CLI                                   |
| babel-eslint                    | Lint modern JavaScript via ESLint                                |
| babel-loader                    | Add Babel support to Webpack                                     |
| babel-preset-react              | Babel preset for working in React                                |
| css-loader                      | Read CSS files via Webpack                                       |
| cssnano                         | Minify CSS                                                       |
| enzyme                          | Simplified JavaScript Testing utilities for React                |
| enzyme-adapter-react-16         | Configure Enzyme to work with React 16                           |
| eslint                          | Lints JavaScript                                                 |
| eslint-loader                   | Run ESLint via Webpack                                           |
| eslint-plugin-import            | Advanced linting of ES6 imports                                  |
| eslint-plugin-react             | Adds additional React-related rules to ESLint                    |
| fetch-mock                      | Mock fetch calls                                                 |
| html-webpack-plugin             | Generate HTML file via webpack                                   |
| http-server                     | Lightweight HTTP server to serve the production build locally    |
| jest                            | Automated testing framework                                      |
| mini-css-extract-plugin         | Extract imported CSS to a separate file via Webpack              |
| node-fetch                      | Make HTTP calls via fetch using Node - Used by fetch-mock        |
| postcss-loader                  | Post-process CSS via Webpack                                     |
| react-test-renderer             | Render React components for testing                              |
| react-testing-library           | Test React components                                            |
| redux-immutable-state-invariant | Warn when Redux state is mutated                                 |
| redux-logger                    | Log Action : Result                                              |
| redux-mock-store                | Mock Redux store for testing                                     |
| rimraf                          | Delete files and folders                                         |
| style-loader                    | Insert imported CSS into app via Webpack                         |
| webpack                         | Bundler with plugin ecosystem and integrated dev server          |
| webpack-bundle-analyzer         | Generate report of what's in the app's production bundle         |
| webpack-cli                     | Run Webpack via the command line                                 |
| webpack-dev-server              | Serve app via Webpack                                            |
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import { connect } from "react-redux";
import { getProjects } from "./redux/actions/projectsActions";

import Header from "./components/Header";
import Footer from "./components/Footer";
import Route from "./components/Route";

const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export function App({ getProjects, projects }) {
  useEffect(() => {
    projects.length === 0 ? getProjects() : null;
  }, []);

  return (
    <>
      <StyledContainer>
        <Header />
        <Route />
        <Footer />
      </StyledContainer>
    </>
  );
}

App.propTypes = {
  getProjects: PropTypes.func.isRequired,
  projects: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
  return {
    projects: state.projects,
  };
}

const mapDispatchToProps = {
  getProjects,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

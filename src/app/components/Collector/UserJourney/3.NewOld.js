/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, NEW_OLD_PAGE } from "../Routes/RoutingLogic";
import { notary } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import NextButton from "../../Common/UserInputs/NextButton";
import Button from "../../Common/UserInputs/Button";
import { NEWOLD, NOTARY, NEW, OLD } from "../../Common/Data/Constants";
import { NotaryCalculation } from "../../Common/Tools/NotaryCalculation";

export function NewOld({ price, newOld, updateProject, updateProgress }) {
  const history = useHistory();
  const { t } = useTranslation();
  const [newOldProject, setNewOldProject] = useState(
    newOld !== "" ? newOld : ""
  );

  const [notaryProject, setNotaryProject] = useState(0);

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(NEW_OLD_PAGE, price);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [price]);

  useEffect(() => {
    calculateNotary();
  }, [price, newOldProject]);

  useEffect(() => {
    updateProgress(NEW_OLD_PAGE);
  }, []);

  const calculateNotary = () => {
    setNotaryProject(NotaryCalculation(newOldProject, price));
  };

  const handleOnClick = (name) => {
    name === t("CollectorNewOldOption1")
      ? setNewOldProject(NEW)
      : setNewOldProject(OLD);
  };

  const handleOnClickNext = () => {
    newOldProject !== "" && notaryProject !== 0
      ? (updateProject(NEWOLD, newOldProject),
        updateProject(NOTARY, notaryProject),
        history.push(notary))
      : null;
  };

  const selected = (buttonName) => {
    return buttonName === newOldProject ? true : false;
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorNewOldTitle")} />
        <Section>
          <Button
            content={t("CollectorNewOldOption1")}
            selected={selected(NEW)}
            onClick={handleOnClick}
          />

          <Button
            content={t("CollectorNewOldOption2")}
            selected={selected(OLD)}
            onClick={handleOnClick}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={newOldProject.length === 0}
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

NewOld.propTypes = {
  newOld: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  updateProject: PropTypes.func.isRequired,
  updateProgress: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    newOld: state.project.newOld,
    price: state.project.price,
  };
}

const mapDispatchToProps = {
  updateProgress,
  updateProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewOld);

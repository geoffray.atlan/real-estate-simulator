/* eslint-disable import/no-named-as-default */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { price } from "../../Route/RoutesConstant";
import { NAME } from "../Routes/RoutingLogic";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import NextButton from "../../Common/UserInputs/NextButton";
import TextInput from "../../Common/UserInputs/TextInput";

export function Name({ name, updateProject, updateProgress }) {
  const history = useHistory();
  const { t } = useTranslation();
  const [disabled, setDisabled] = useState(true);
  const [nameProject, setNameProject] = useState(name ? name : "");

  useEffect(() => {
    setDisabled(nameProject.length === 0);
  }, [nameProject]);

  useEffect(() => {
    updateProgress(NAME);
  }, []);

  function handleChange(event) {
    const { value } = event.target;
    setNameProject(value);
  }

  const handleOnClickNext = () => {
    if (!disabled) {
      updateProject(NAME, nameProject);
      history.push(price);
    }
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorNameTitle")} />

        <Section>
          <TextInput
            value={nameProject}
            onChange={handleChange}
            autofocus={true}
          />
        </Section>
      </TitleAndInput>

      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={disabled}
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

Name.propTypes = {
  name: PropTypes.string.isRequired,
  updateProject: PropTypes.func.isRequired,
  updateProgress: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    name: state.project.name,
  };
}

const mapDispatchToProps = {
  updateProject,
  updateProgress,
};

export default connect(mapStateToProps, mapDispatchToProps)(Name);

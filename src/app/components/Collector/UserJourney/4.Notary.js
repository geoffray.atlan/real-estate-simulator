/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, NOTARY_PAGE } from "../Routes/RoutingLogic";
import { propertytype } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import NextButton from "../../Common/UserInputs/NextButton";
import InputSlider from "../../Common/UserInputs/Slider";
import { NOTARY } from "../../Common/Data/Constants";

export function Notary({ notary, updateProject, newOld, updateProgress }) {
  const history = useHistory();
  const { t } = useTranslation();
  const [notaryProject, setNotaryProject] = useState(notary !== 0 ? notary : 0);

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(NOTARY_PAGE, newOld);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [newOld]);

  useEffect(() => {
    updateProgress(NOTARY_PAGE);
  }, []);


  const handleValueChange = (newValue) => {
    setNotaryProject(newValue);
  };

  const handleOnClickNext = () => {
    updateProject(NOTARY, notaryProject);
    history.push(propertytype);
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorNotaryTitle")} />
        <Section>
          <InputSlider
            min={MinMax.notary.min}
            max={MinMax.notary.max}
            valueIs={notaryProject}
            onChange={handleValueChange}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={
            notaryProject <= MinMax.notary.min ||
            notaryProject > MinMax.notary.max
          }
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

Notary.propTypes = {
  notary: PropTypes.number.isRequired,
  newOld: PropTypes.string.isRequired,
  updateProgress: PropTypes.func.isRequired,
  updateProject: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    newOld: state.project.newOld,
    notary: state.project.notary,
  };
}

const mapDispatchToProps = {
  updateProgress,
  updateProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(Notary);

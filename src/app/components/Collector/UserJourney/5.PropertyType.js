/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, PROPERTY_TYPE_PAGE } from "../Routes/RoutingLogic";
import { numberroom } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import NextButton from "../../Common/UserInputs/NextButton";
import Button from "../../Common/UserInputs/Button";
import { PROPERTY_TYPE, APARTMENT, HOUSE } from "../../Common/Data/Constants";

export function PropertyType({ notary, propertyType, updateProject, updateProgress }) {
  const history = useHistory();
  const { t } = useTranslation();

  const [propertyTypeProject, setPropertyTypeProject] = useState(
    propertyType !== "" ? propertyType : ""
  );

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(PROPERTY_TYPE_PAGE, notary);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [notary]);

  useEffect(() => {
    updateProgress(PROPERTY_TYPE_PAGE);
  }, []);


  const handleOnClick = (name) => {
    name === t("CollectorPropertyTypeOption1")
      ? setPropertyTypeProject(APARTMENT)
      : setPropertyTypeProject(HOUSE);
  };

  const handleOnClickNext = () => {
    updateProject(PROPERTY_TYPE, propertyTypeProject);
    history.push(numberroom);
  };

  const selected = (buttonName) => {
    return buttonName === propertyTypeProject ? true : false;
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorPropertyTypeTitle")} />
        <Section>
          <Button
            content={t("CollectorPropertyTypeOption1")}
            selected={selected(APARTMENT)}
            onClick={handleOnClick}
          />

          <Button
            content={t("CollectorPropertyTypeOption2")}
            selected={selected(HOUSE)}
            onClick={handleOnClick}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={propertyTypeProject.length === 0}
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

PropertyType.propTypes = {
  propertyType: PropTypes.string.isRequired,
  updateProgress: PropTypes.func.isRequired,
  notary: PropTypes.number.isRequired,
  updateProject: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    propertyType: state.project.propertyType,
    notary: state.project.notary,
  };
}

const mapDispatchToProps = {
  updateProgress,
  updateProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(PropertyType);

/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, LOAN_DURATION_PAGE } from "../Routes/RoutingLogic";
import { projectId } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { saveProject } from "../../../redux/actions/projectsActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import NextButton from "../../Common/UserInputs/NextButton";
import InputSlider from "../../Common/UserInputs/Slider";
import { LOAN_DURATION, CREATE } from "../../Common/Data/Constants";

export function Area({
  insuranceRate,
  loanDuration,
  project,
  projects,
  updateProject,
  saveProject,
  updateProgress,
}) {
  const history = useHistory();
  const { t } = useTranslation();
  const [constructionProject, setConstructionProject] = useState(
    loanDuration !== 0 ? loanDuration : 0
  );
  const [projectToSave, setProjectToSave] = useState({});

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(LOAN_DURATION_PAGE, insuranceRate);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [insuranceRate]);

  useEffect(() => {
    setProjectToSave(project);
  }, [constructionProject]);

  useEffect(() => {
    updateProgress(LOAN_DURATION_PAGE);
  }, []);

  useEffect(() => {
    setProjectToSave(project);
  }, [project]);

  const handleValueChange = (newValue) => {
    setConstructionProject(newValue);
    updateProject(LOAN_DURATION, newValue);
  };

  async function handleOnClickNext() {
    try {
      await saveProject(projectToSave, CREATE);
    } catch (error) {
      console.log(error);
    }
    history.push(projectId + projects.length);
  }

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorloanDurationTitle")} />
        <Section>
          <InputSlider
            min={MinMax.loanDuration.min}
            max={MinMax.loanDuration.max}
            valueIs={constructionProject}
            onChange={handleValueChange}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonFinish")}
          isDisabled={
            constructionProject <= MinMax.loanDuration.min ||
            constructionProject > MinMax.loanDuration.max
          }
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

Area.propTypes = {
  loanDuration: PropTypes.number.isRequired,
  insuranceRate: PropTypes.number.isRequired,
  project: PropTypes.object.isRequired,
  projects: PropTypes.array.isRequired,
  updateProject: PropTypes.func.isRequired,
  updateProgress: PropTypes.func.isRequired,
  saveProject: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    loanDuration: state.project.loanDuration,
    insuranceRate: state.project.insuranceRate,
    project: state.project,
    projects: state.projects,
  };
}

const mapDispatchToProps = {
  updateProject,
  updateProgress,
  saveProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(Area);

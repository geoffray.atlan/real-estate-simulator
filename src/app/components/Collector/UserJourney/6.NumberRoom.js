/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, NUMBER_ROOM_PAGE } from "../Routes/RoutingLogic";
import { construction } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import NextButton from "../../Common/UserInputs/NextButton";
import Picklist from "../../Common/UserInputs/Picklist/Picklist";
import { NUMBER_ROOM } from "../../Common/Data/Constants";

export function NumberRoom({ propertyType, numberRoom, updateProject, updateProgress }) {
  const history = useHistory();
  const { t } = useTranslation();
  const [numberRoomProject, setNumberRoomProject] = useState(
    numberRoom !== 0 ? numberRoom : 0
  );

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(NUMBER_ROOM_PAGE, propertyType);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [propertyType]);

  useEffect(() => {
    updateProgress(NUMBER_ROOM_PAGE);
  }, []);


  const handleOnClick = (name) => {
    setNumberRoomProject(Number(name));
  };

  const handleOnClickNext = () => {
    updateProject(NUMBER_ROOM, numberRoomProject);
    history.push(construction);
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorNumberRoomTitle")} />
        <Section>
          <Picklist
            name="Number of Rooms"
            value={numberRoomProject}
            onChange={handleOnClick}
            options={MinMax.numberOfRoom.options}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={
            numberRoomProject < MinMax.numberOfRoom.min ||
            numberRoomProject > MinMax.numberOfRoom.max
          }
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

NumberRoom.propTypes = {
  propertyType: PropTypes.string.isRequired,
  numberRoom: PropTypes.number.isRequired,
  updateProgress: PropTypes.func.isRequired,
  updateProject: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    numberRoom: state.project.numberRoom,
    propertyType: state.project.propertyType,
  };
}

const mapDispatchToProps = {
  updateProgress,
  updateProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(NumberRoom);

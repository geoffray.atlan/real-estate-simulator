/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, INSURANCE_RATE_PAGE } from "../Routes/RoutingLogic";
import { loanDuration } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import NextButton from "../../Common/UserInputs/NextButton";
import InputSlider from "../../Common/UserInputs/Slider";
import { INSURANCE_RATE, PERCENT_SYMBOLE } from "../../Common/Data/Constants";

export function PurchaseDate({
  insuranceRate,
  interestRate,
  updateProject,
  updateProgress,
}) {
  const history = useHistory();
  const { t } = useTranslation();
  const [insuranceRateProject, setInsuranceRateProject] = useState(
    insuranceRate !== 0 ? insuranceRate : 0
  );

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(INSURANCE_RATE_PAGE, interestRate);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [interestRate]);

  useEffect(() => {
    updateProgress(INSURANCE_RATE_PAGE);
  }, []);

  const handleValueChange = (newValue) => {
    setInsuranceRateProject(newValue);
  };

  const handleOnClickNext = () => {
    updateProject(INSURANCE_RATE, insuranceRateProject);
    history.push(loanDuration);
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorInsuranceRateTitle")} />
        <Section>
          <InputSlider
            min={MinMax.insuranceRate.min}
            max={MinMax.insuranceRate.max}
            valueIs={insuranceRateProject}
            onChange={handleValueChange}
            prefix={PERCENT_SYMBOLE}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={
            insuranceRateProject <= MinMax.insuranceRate.min ||
            insuranceRateProject > MinMax.insuranceRate.max
          }
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

PurchaseDate.propTypes = {
  insuranceRate: PropTypes.number.isRequired,
  interestRate: PropTypes.number.isRequired,
  updateProgress: PropTypes.func.isRequired,
  updateProject: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    interestRate: state.project.interestRate,
    insuranceRate: state.project.insuranceRate,
  };
}

const mapDispatchToProps = {
  updateProgress,
  updateProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseDate);

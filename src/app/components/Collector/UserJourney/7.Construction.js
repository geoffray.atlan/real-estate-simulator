/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { RoutingLogic, CONSTRUCTION_PAGE } from "../Routes/RoutingLogic";
import { furnitureCosts } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import InputSlider from "../../Common/UserInputs/Slider";
import NextButton from "../../Common/UserInputs/NextButton";
import { CONSTRUCTION } from "../../Common/Data/Constants";

export function Construction({ numberRoom, construction, updateProject , updateProgress}) {
  const history = useHistory();
  const { t } = useTranslation();
  const [constructionProject, setConstructionProject] = useState(
    construction !== 0 ? construction : 0
  );

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(CONSTRUCTION_PAGE, numberRoom);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [numberRoom]);

  useEffect(() => {
    updateProgress(CONSTRUCTION_PAGE);
  }, []);


  const handleValueChange = (newValue) => {
    setConstructionProject(newValue);
  };

  function handleOnClickNext() {
    updateProject(CONSTRUCTION, constructionProject);
    history.push(furnitureCosts);
  }

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorConstructionTitle")} />
        <Section>
          <InputSlider
            min={MinMax.construction.min}
            max={MinMax.construction.max}
            valueIs={constructionProject}
            onChange={handleValueChange}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={
            constructionProject <= MinMax.construction.min ||
            constructionProject > MinMax.construction.max
          }
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

Construction.propTypes = {
  construction: PropTypes.number.isRequired,
  numberRoom: PropTypes.number.isRequired,
  updateProject: PropTypes.func.isRequired,
  project: PropTypes.object.isRequired,
  updateProgress: PropTypes.func.isRequired,
  projects: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
  return {
    construction: state.project.construction,
    numberRoom: state.project.numberRoom,
    project: state.project,
    projects: state.projects,
  };
}

const mapDispatchToProps = {
  updateProgress,
  updateProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(Construction);

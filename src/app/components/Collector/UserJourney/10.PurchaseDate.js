/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, PURCHASE_DATE_PAGE } from "../Routes/RoutingLogic";
import { rent } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import DatePicker from "../../Common/UserInputs/DatePicker";
import NextButton from "../../Common/UserInputs/NextButton";
import { PURCHASE_DATE } from "../../Common/Data/Constants";

export function PurchaseDate({ purchaseDate, area, updateProject, updateProgress }) {
  const history = useHistory();
  const { t } = useTranslation();
  const [purchaseDateProject, setPurchaseDateProject] = useState(
    purchaseDate !== 0 ? purchaseDate : (new Date(Date.now()).getTime())
  );

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(PURCHASE_DATE_PAGE, area);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [area]);

  useEffect(() => {
    updateProgress(PURCHASE_DATE_PAGE);
  }, []);


  const handleValueChange = (newValue) => {
    setPurchaseDateProject(newValue);
  };

  const handleOnClickNext = () => {
    updateProject(PURCHASE_DATE, purchaseDateProject);
    history.push(rent);
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorPurchaseDateTitle")} />
        <Section>
          <DatePicker
            onChange={handleValueChange}
            value={new Date(purchaseDateProject)}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={purchaseDateProject.length === 0}
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

PurchaseDate.propTypes = {
  purchaseDate: PropTypes.number.isRequired,
  area: PropTypes.number.isRequired,
  updateProgress: PropTypes.func.isRequired,
  updateProject: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    area: state.project.area,
    purchaseDate: state.project.purchaseDate,
  };
}

const mapDispatchToProps = {
  updateProgress,
  updateProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseDate);

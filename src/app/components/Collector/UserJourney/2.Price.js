/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { newold } from "../../Route/RoutesConstant";
import { RoutingLogic, PRICE_PAGE } from "../Routes/RoutingLogic";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import InputSlider from "../../Common/UserInputs/Slider";
import NextButton from "../../Common/UserInputs/NextButton";
import { PRICE } from "../../Common/Data/Constants";

export function Price({ name, price, updateProject, updateProgress }) {
  const history = useHistory();
  const { t } = useTranslation();
  const [priceProject, setPriceProject] = useState(price ? price : 0);
  const [disabled, setDisabled] = useState(true);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(PRICE_PAGE, name);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [name]);

  useEffect(() => {
    updateProgress(PRICE_PAGE);
  }, []);
  
  useEffect(() => {
    handleError(
      priceProject,
      MinMax.price.min,
      MinMax.price.max
    );
    setDisabled(
      priceProject < MinMax.price.min ||
        priceProject > MinMax.price.max
    );
  }, [priceProject]);

  const handleValueChange = (newValue) => {
    setPriceProject(newValue);
  };

  const handleOnClickNext = () => {
    if (!disabled) {
      updateProject(PRICE, priceProject);
      history.push(newold);
    }
  };

  const handleError = (value, min, max) => {
    if (value < min) {
      setError(true);
      setErrorMessage(t("ErrorMessagesValueTooLow"));
    }
    if (value > max) {
      setError(true);
      setErrorMessage(t("ErrorMessagesValueTooHigh"));
    }
    if (value < max && value > min) {
      setError(false);
      setErrorMessage("");
    }
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorPriceTitle")} />
        <Section>
          <InputSlider
            min={MinMax.price.min}
            max={MinMax.price.max}
            helperText={errorMessage}
            valueIs={priceProject}
            onChange={handleValueChange}
            errorMessage={error ? errorMessage : null}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={disabled}
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

Price.propTypes = {
  price: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  updateProject: PropTypes.func.isRequired,
  updateProgress: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    price: state.project.price,
    name: state.project.name,
  };
}

const mapDispatchToProps = {
  updateProject,
  updateProgress,
};

export default connect(mapStateToProps, mapDispatchToProps)(Price);

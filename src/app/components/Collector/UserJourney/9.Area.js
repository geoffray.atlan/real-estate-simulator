/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { RoutingLogic, AREA_PAGE } from "../Routes/RoutingLogic";
import { purchasedate } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import NextButton from "../../Common/UserInputs/NextButton";
import InputSlider from "../../Common/UserInputs/Slider";
import { AREA } from "../../Common/Data/Constants";

export function Area({ area, updateProject, furnitureCosts, updateProgress }) {
  const history = useHistory();
  const { t } = useTranslation();
  const [areaProject, setAreaProject] = useState(area !== 0 ? area : 0);

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(AREA_PAGE, furnitureCosts);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [furnitureCosts]);

  useEffect(() => {
    updateProgress(AREA_PAGE);
  }, []);


  const handleValueChange = (newValue) => {
    setAreaProject(newValue);
  };

  const handleOnClickNext = () => {
    updateProject(AREA, areaProject);
    history.push(purchasedate);
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorAreaTitle")} />
        <Section>
          <InputSlider
            min={MinMax.area.min}
            max={MinMax.area.max}
            valueIs={areaProject}
            prefix={t("_WordAreaUnit")}
            onChange={handleValueChange}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={
            areaProject <= MinMax.area.min ||
            areaProject > MinMax.area.max
          }
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

Area.propTypes = {
  area: PropTypes.number.isRequired,
  furnitureCosts: PropTypes.number.isRequired,
  updateProgress: PropTypes.func.isRequired,
  updateProject: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    furnitureCosts: state.project.furnitureCosts,
    area: state.project.area,
  };
}

const mapDispatchToProps = {
  updateProgress,
  updateProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(Area);

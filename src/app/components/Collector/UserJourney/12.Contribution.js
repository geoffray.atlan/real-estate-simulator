/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, CONTRIBUTION_PAGE } from "../Routes/RoutingLogic";
import { interestRate } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import NextButton from "../../Common/UserInputs/NextButton";
import InputSlider from "../../Common/UserInputs/Slider";
import { CONTRIBUTION } from "../../Common/Data/Constants";

export function Contribution({
  rent,
  contribution,
  updateProject,
  updateProgress,
}) {
  const history = useHistory();
  const { t } = useTranslation();
  const [contributionProject, setContributionProject] = useState(
    contribution !== 0 ? contribution : 0
  );

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(CONTRIBUTION_PAGE, rent);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [rent]);

  useEffect(() => {
    updateProgress(CONTRIBUTION_PAGE);
  }, []);

  const handleValueChange = (newValue) => {
    setContributionProject(newValue);
  };

  async function handleOnClickNext() {
    updateProject(CONTRIBUTION, contributionProject);
    history.push(interestRate);
  }

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorContributionTitle")} />
        <Section>
          <InputSlider
            min={MinMax.contribution.min}
            max={MinMax.contribution.max}
            valueIs={contributionProject}
            onChange={handleValueChange}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonFinish")}
          isDisabled={
            contributionProject <= MinMax.contribution.min ||
            contributionProject > MinMax.contribution.max
          }
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

Contribution.propTypes = {
  contribution: PropTypes.number.isRequired,
  rent: PropTypes.number.isRequired,
  updateProject: PropTypes.func.isRequired,
  updateProgress: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    contribution: state.project.contribution,
    rent: state.project.rent,
  };
}

const mapDispatchToProps = {
  updateProject,
  updateProgress,
};

export default connect(mapStateToProps, mapDispatchToProps)(Contribution);

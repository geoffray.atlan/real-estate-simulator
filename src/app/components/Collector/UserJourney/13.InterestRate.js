/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, INTEREST_RATE_PAGE } from "../Routes/RoutingLogic";
import { insuranceRate } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import NextButton from "../../Common/UserInputs/NextButton";
import InputSlider from "../../Common/UserInputs/Slider";
import { INTEREST_RATE, PERCENT_SYMBOLE } from "../../Common/Data/Constants";

export function PurchaseDate({
  interestRate,
  contribution,
  updateProject,
  updateProgress,
}) {
  const history = useHistory();
  const { t } = useTranslation();
  const [interestRateProject, setInterestRateProject] = useState(
    interestRate !== 0 ? interestRate : 0
  );

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(INTEREST_RATE_PAGE, contribution);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [contribution]);

  useEffect(() => {
    updateProgress(INTEREST_RATE_PAGE);
  }, []);

  const handleValueChange = (newValue) => {
    setInterestRateProject(newValue);
  };

  const handleOnClickNext = () => {
    updateProject(INTEREST_RATE, interestRateProject);
    history.push(insuranceRate);
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorInterestRateTitle")} />
        <Section>
          <InputSlider
            min={MinMax.interestRate.min}
            max={MinMax.interestRate.max}
            valueIs={interestRateProject}
            prefix={PERCENT_SYMBOLE}
            onChange={handleValueChange}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={
            interestRateProject <= MinMax.interestRate.min ||
            interestRateProject > MinMax.interestRate.max
          }
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

PurchaseDate.propTypes = {
  interestRate: PropTypes.number.isRequired,
  contribution: PropTypes.number.isRequired,
  updateProgress: PropTypes.func.isRequired,
  updateProject: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    contribution: state.project.contribution,
    interestRate: state.project.interestRate,
  };
}

const mapDispatchToProps = {
  updateProgress,
  updateProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseDate);

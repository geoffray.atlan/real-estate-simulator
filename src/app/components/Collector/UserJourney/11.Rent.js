/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, RENT_PAGE } from "../Routes/RoutingLogic";
import { contribution } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import NextButton from "../../Common/UserInputs/NextButton";
import InputSlider from "../../Common/UserInputs/Slider";
import { RENT } from "../../Common/Data/Constants";

export function Rent({
  purchaseDate,
  rent,
  updateProject,
  updateProgress,
}) {
  const history = useHistory();
  const { t } = useTranslation();
  const [rentProject, setRentProject] = useState(
    rent !== 0 ? rent : 0
  );

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(RENT_PAGE, purchaseDate);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [purchaseDate]);

  useEffect(() => {
    updateProgress(RENT_PAGE);
  }, []);

  const handleValueChange = (newValue) => {
    setRentProject(newValue);
  };
  
  async function handleOnClickNext() {
    updateProject(RENT, rentProject);
    history.push(contribution);
  }

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorRentTitle")} />
        <Section>
          <InputSlider
            min={MinMax.rent.min}
            max={MinMax.rent.max}
            valueIs={rentProject}
            onChange={handleValueChange}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonFinish")}
          isDisabled={
            rentProject <= MinMax.rent.min ||
            rentProject > MinMax.rent.max
          }
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

Rent.propTypes = {
  rent: PropTypes.number.isRequired,
  purchaseDate: PropTypes.number.isRequired,
  updateProject: PropTypes.func.isRequired,
  updateProgress: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    rent: state.project.rent,
    purchaseDate: state.project.purchaseDate,
  };
}

const mapDispatchToProps = {
  updateProject,
  updateProgress,
};

export default connect(mapStateToProps, mapDispatchToProps)(Rent);

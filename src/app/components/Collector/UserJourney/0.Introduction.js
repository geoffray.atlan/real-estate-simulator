/* eslint-disable import/no-named-as-default */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";
import { name } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProgress } from "../../../redux/actions/userInteractionActions";
import { INTRODUCTION_PAGE } from "../Routes/RoutingLogic";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import { useTranslation } from "react-i18next";
import NextButton from "../../Common/UserInputs/NextButton";
import { CustomTypography } from "../../Common/Style/Typography";

export function Name({ updateProgress }) {
  const history = useHistory();
  const { t } = useTranslation();
  
  useEffect(() => {
    updateProgress(INTRODUCTION_PAGE);
  }, []);

  const handleOnClickNext = () => {
    history.push(name);
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorIntroductionTitle")} />

        <Section>
          <CustomTypography
            value={t("CollectorIntroductionContent")}
            variant="body2"
          />
        </Section>
      </TitleAndInput>

      <Div>
        <NextButton
          isDisabled={false}
          content={t("NextButtonNext")}
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

Name.propTypes = {
  updateProgress: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  updateProgress,
};

export default connect(null, mapDispatchToProps)(Name);

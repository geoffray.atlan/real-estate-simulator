/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { RoutingLogic, FURNITURE_COSTS_PAGE } from "../Routes/RoutingLogic";
import { area } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import { updateProject } from "../../../redux/actions/projectActions";
import { updateProgress } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title, Section, Div } from "../Page/Collector.style";
import MinMax from "../../Common/Data/MinMax";
import NextButton from "../../Common/UserInputs/NextButton";
import InputSlider from "../../Common/UserInputs/Slider";
import { FURNITURE_COSTS } from "../../Common/Data/Constants";

export function FurnitureCosts({
  furnitureCosts,
  updateProject,
  construction,
  updateProgress,
}) {
  const history = useHistory();
  const { t } = useTranslation();
  const [furnitureCostsProject, setFurnitureCostsProject] = useState(
    furnitureCosts !== 0 ? furnitureCosts : 0
  );

  useEffect(() => {
    let allowedToDisplayPage = RoutingLogic(FURNITURE_COSTS_PAGE, construction);
    allowedToDisplayPage ? history.push(allowedToDisplayPage) : null;
  }, [construction]);

  useEffect(() => {
    updateProgress(FURNITURE_COSTS_PAGE);
  }, []);

  const handleValueChange = (newValue) => {
    setFurnitureCostsProject(newValue);
  };

  const handleOnClickNext = () => {
    updateProject(FURNITURE_COSTS, furnitureCostsProject);
    history.push(area);
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("CollectorFurnitureCostsTitle")} />
        <Section>
          <InputSlider
            min={MinMax.furnitureCosts.min}
            max={MinMax.furnitureCosts.max}
            valueIs={furnitureCostsProject}
            onChange={handleValueChange}
          />
        </Section>
      </TitleAndInput>
      <Div>
        <NextButton
          content={t("NextButtonNext")}
          isDisabled={
            furnitureCostsProject <= MinMax.price.min ||
            furnitureCostsProject > MinMax.price.max
          }
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

FurnitureCosts.propTypes = {
  furnitureCosts: PropTypes.number.isRequired,
  construction: PropTypes.number.isRequired,
  updateProject: PropTypes.func.isRequired,  updateProgress: PropTypes.func.isRequired,
}


function mapStateToProps(state) {
  return {
    construction: state.project.construction,
    furnitureCosts: state.project.furnitureCosts,
  };
}

const mapDispatchToProps = {
  updateProgress,
  updateProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(FurnitureCosts);

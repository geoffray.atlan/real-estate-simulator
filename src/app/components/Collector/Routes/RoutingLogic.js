import PropTypes from "prop-types";
import { NEW_PROJECT, EXISTING_PROJECT } from "../../Common/Data/Constants";
import {
  introduction,
  name,
  price,
  newold,
  notary,
  propertytype,
  numberroom,
  construction,
  furnitureCosts,
  area,
  purchasedate,
  rent,
  contribution,
  interestRate,
  insuranceRate,
  loanDuration,
} from "../../Route/RoutesConstant";
import { connect } from "react-redux";

export const INTRODUCTION_PAGE = "INTRODUCTION_PAGE";
export const NAME = "NAME";
export const PRICE_PAGE = "PRICE_PAGE";
export const NEW_OLD_PAGE = "NEW_OLD_PAGE";
export const NOTARY_PAGE = "NOTARY_PAGE";
export const PROPERTY_TYPE_PAGE = "PROPERTY_TYPE_PAGE";
export const NUMBER_ROOM_PAGE = "NUMBER_ROOM_PAGE";
export const CONSTRUCTION_PAGE = "CONSTRUCTION_PAGE";
export const FURNITURE_COSTS_PAGE = "FURNITURE_COSTS_PAGE";
export const AREA_PAGE = "AREA_PAGE";
export const PURCHASE_DATE_PAGE = "PURCHASE_DATE_PAGE";
export const RENT_PAGE = "RENT_PAGE";
export const CONTRIBUTION_PAGE = "CONTRIBUTION_PAGE";
export const INTEREST_RATE_PAGE = "INTEREST_RATE_PAGE";
export const INSURANCE_RATE_PAGE = "INSURANCE_RATE_PAGE";
export const LOAN_DURATION_PAGE = "LOAN_DURATION_PAGE";

export const namePagesNewProjectList = [
  INTRODUCTION_PAGE,
  NAME,
  PRICE_PAGE,
  NEW_OLD_PAGE,
  NOTARY_PAGE,
  PROPERTY_TYPE_PAGE,
  NUMBER_ROOM_PAGE,
  CONSTRUCTION_PAGE,
  FURNITURE_COSTS_PAGE,
  AREA_PAGE,
  PURCHASE_DATE_PAGE,
  RENT_PAGE,
  CONTRIBUTION_PAGE,
  INTEREST_RATE_PAGE,
  INSURANCE_RATE_PAGE,
  LOAN_DURATION_PAGE,
];

export const namePagesExistingProjectList = [
  INTRODUCTION_PAGE,
  NAME,
  PRICE_PAGE,
  NEW_OLD_PAGE,
  NOTARY_PAGE,
  PROPERTY_TYPE_PAGE,
  NUMBER_ROOM_PAGE,
  CONSTRUCTION_PAGE,
  FURNITURE_COSTS_PAGE,
  AREA_PAGE,
  PURCHASE_DATE_PAGE,
  RENT_PAGE,
  CONTRIBUTION_PAGE,
  INTEREST_RATE_PAGE,
  INSURANCE_RATE_PAGE,
  LOAN_DURATION_PAGE,
];
const namePagesNewProject = [
  { page: INTRODUCTION_PAGE, route: introduction },
  { page: NAME, route: name },
  { page: PRICE_PAGE, route: price },
  { page: NEW_OLD_PAGE, route: newold },
  { page: NOTARY_PAGE, route: notary },
  { page: PROPERTY_TYPE_PAGE, route: propertytype },
  { page: NUMBER_ROOM_PAGE, route: numberroom },
  { page: CONSTRUCTION_PAGE, route: construction },
  { page: FURNITURE_COSTS_PAGE, route: furnitureCosts },
  { page: AREA_PAGE, route: area },
  { page: PURCHASE_DATE_PAGE, route: purchasedate },
  { page: RENT_PAGE, route: rent },
  { page: CONTRIBUTION_PAGE, route: contribution },
  { page: INTEREST_RATE_PAGE, route: interestRate },
  { page: INSURANCE_RATE_PAGE, route: insuranceRate },
  { page: LOAN_DURATION_PAGE, route: loanDuration },
];

const namePagesExistingProject = [
  { page: INTRODUCTION_PAGE, route: introduction },
  { page: NAME, route: name },
  { page: PRICE_PAGE, route: price },
  { page: NEW_OLD_PAGE, route: newold },
  { page: NOTARY_PAGE, route: notary },
  { page: PROPERTY_TYPE_PAGE, route: propertytype },
  { page: NUMBER_ROOM_PAGE, route: numberroom },
  { page: CONSTRUCTION_PAGE, route: construction },
  { page: FURNITURE_COSTS_PAGE, route: furnitureCosts },
  { page: AREA_PAGE, route: area },
  { page: PURCHASE_DATE_PAGE, route: purchasedate },
  { page: RENT_PAGE, route: rent },
  { page: CONTRIBUTION_PAGE, route: contribution },
  { page: INTEREST_RATE_PAGE, route: interestRate },
  { page: INSURANCE_RATE_PAGE, route: insuranceRate },
  { page: LOAN_DURATION_PAGE, route: loanDuration },
];

export function RoutingLogic(namePage, value, newProjectType) {
  let namePages = [];
  if (newProjectType === NEW_PROJECT) {
    namePages = namePagesNewProject;
  } else if (newProjectType === EXISTING_PROJECT) {
    namePages = namePagesExistingProject;
  }

  let pageToGo = false;
  const previousPage = () => {
    let previous = namePages.find((page) => page.page === namePage);
    previous = namePages[namePages.indexOf(previous) - 1].route;
    return previous;
  };
  if (
    (typeof value === "string" && value === "") ||
    (typeof value === "number" && value === 0)
  ) {
    pageToGo = previousPage();
  }
  return pageToGo;
}

RoutingLogic.propTypes = {
  namePage: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  newProjectType: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  return {
    newProjectType: state.userInteraction.newProjectType,
  };
}

export default connect(mapStateToProps, null)(RoutingLogic);

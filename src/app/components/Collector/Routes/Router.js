/* eslint-disable import/no-named-as-default */
import React from "react";
import { Route } from "react-router-dom";

import Introduction from "../UserJourney/0.Introduction";
import Name from "../UserJourney/1.Name";
import Price from "../UserJourney/2.Price";
import NewOld from "../UserJourney/3.NewOld";
import Notary from "../UserJourney/4.Notary";
import PropertyType from "../UserJourney/5.PropertyType";
import NumberRoom from "../UserJourney/6.NumberRoom";
import Construction from "../UserJourney/7.Construction";
import FurnitureCosts from "../UserJourney/8.FurnitureCosts";
import Area from "../UserJourney/9.Area";
import PurchaseDate from "../UserJourney/10.PurchaseDate";
import Rent from "../UserJourney/11.Rent";
import Contribution from "../UserJourney/12.Contribution";
import InterestRate from "../UserJourney/13.InterestRate";
import InsuranceRate from "../UserJourney/14.InsuranceRate";
import LoanDuration from "../UserJourney/15.LoanDuration";
import {
  introduction,
  name,
  price,
  newold,
  notary,
  propertytype,
  numberroom,
  construction,
  furnitureCosts,
  area,
  purchasedate,
  rent,
  contribution,
  interestRate,
  insuranceRate,
  loanDuration,
} from "../../Route/RoutesConstant";

export function Router() {
  return (
    <>
      <Route path={introduction} component={Introduction} />
      <Route path={name} component={Name} />
      <Route path={price} component={Price} />
      <Route path={newold} component={NewOld} />
      <Route path={notary} component={Notary} />
      <Route path={propertytype} component={PropertyType} />
      <Route path={numberroom} component={NumberRoom} />
      <Route path={construction} component={Construction} />
      <Route path={furnitureCosts} component={FurnitureCosts} />
      <Route path={area} component={Area} />
      <Route path={purchasedate} component={PurchaseDate} />
      <Route path={rent} component={Rent} />
      <Route path={contribution} component={Contribution} />
      <Route path={interestRate} component={InterestRate} />
      <Route path={insuranceRate} component={InsuranceRate} />
      <Route path={loanDuration} component={LoanDuration} />
    </>
  );
}

export default Router;

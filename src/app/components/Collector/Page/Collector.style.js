import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { CustomTypography } from "../../Common/Style/Typography";
import theme from "../../Common/Style/Theme";

const StyledContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  -webkit-box-pack: start;
  justify-content: flex-start;
  min-height: 40rem;
  position: relative;
  margin: 1rem 6rem;

  @media (max-width: ${theme.breakpoints.values.xs}px) {
    margin: 0.5rem 1.5rem;
  }

  @media (min-width: ${theme.breakpoints.values.xs}px) and (max-width: ${theme
      .breakpoints.values.sm}px) {
    margin: 0.5rem 2rem;
  }

  @media (min-width: ${theme.breakpoints.values.sm}px) and (max-width: ${theme
      .breakpoints.values.md}px) {
    margin: 0.8rem 4rem;
  }
`;

export function Container(props) {
  return <StyledContainer>{props.children}</StyledContainer>;
}

const StyledTitleAndInput = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  -webkit-box-pack: start;
  justify-content: flex-start;
  min-height: 40rem;
  position: relative;
  margin: 1rem 6rem;

  @media (max-width: ${theme.breakpoints.values.xs}px) {
    margin: 0.5rem 1.5rem;
  }

  @media (min-width: ${theme.breakpoints.values.xs}px) and (max-width: ${theme
      .breakpoints.values.sm}px) {
    margin: 0.5rem 2rem;
  }

  @media (min-width: ${theme.breakpoints.values.sm}px) and (max-width: ${theme
      .breakpoints.values.md}px) {
    margin: 0.8rem 4rem;
  }
`;

export function TitleAndInput(props) {
  return <StyledTitleAndInput>{props.children}</StyledTitleAndInput>;
}

const StyledHeader = styled.header`
  text-align: center;
  margin: 3.75rem auto;
`;

export function Title({ title }) {
  return (
    <>
      <StyledHeader>
        <CustomTypography value={title} variant={"h2"} />
      </StyledHeader>
    </>
  );
}

const StyledSection = styled.section`
  max-width: 30rem;
  margin: 0px auto;
`;

export function Section(props) {
  return <StyledSection>{props.children}</StyledSection>;
}

const StyledDiv = styled.div`
  position: sticky;
  bottom: 2rem;
  margin: 1rem 6rem;

  @media (max-width: ${theme.breakpoints.values.xs}px) {
    margin: 0.5rem 1.5rem;
  }

  @media (min-width: ${theme.breakpoints.values.xs}px) and (max-width: ${theme
      .breakpoints.values.sm}px) {
    margin: 0.5rem 2rem;
  }

  @media (min-width: ${theme.breakpoints.values.sm}px) and (max-width: ${theme
      .breakpoints.values.md}px) {
    margin: 0.8rem 4rem;
  }
`;

export function Div(props) {
  return <StyledDiv>{props.children}</StyledDiv>;
}

Container.propTypes = {
  children: PropTypes.node,
};

TitleAndInput.propTypes = {
  children: PropTypes.node,
};

Title.propTypes = {
  title: PropTypes.string.isRequired,
};

Section.propTypes = {
  children: PropTypes.node,
};

Div.propTypes = {
  children: PropTypes.node,
};

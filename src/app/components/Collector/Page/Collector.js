/* eslint-disable import/no-named-as-default */
import React from "react";
import Router from "../Routes/Router";
import PropTypes from "prop-types";
import { Container } from "./Collector.style";
import ProgressBar from "../../Common/Style/ProgressBar";
import { connect } from "react-redux";

export function Collector({ progress = 0 }) {
  return (
    <>
      <ProgressBar progress={progress} />
      <Container>
        <Router />
      </Container>
    </>
  );
}

Collector.propTypes = {
  progress: PropTypes.number.isRequired,
};

function mapStateToProps(state) {
  return {
    progress: state.userInteraction.progress,
  };
}

export default connect(mapStateToProps)(Collector);

import React from "react";
import { Line } from "react-chartjs-2";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { CustomTypography } from "../Common/Style/Typography";

const LineChart = ({ graphData }) => {
  const { t } = useTranslation();
  const {
    balancesYearly,
    equitiesYearly,
    insuranceYearly,
    interestsYearly,
    year,
  } = graphData;

  let config = {
    type: "line",
    data: {
      labels: year,
      datasets: [
        {
          data: equitiesYearly,
          label: t("_WordEquity"),
          backgroundColor: "green",
          borderColor: "green",
          fill: true,
        },
        {
          data: interestsYearly,
          label: t("_WordInterest"),
          fill: true,
          backgroundColor: "red",
          borderColor: "red",
        },
        {
          data: insuranceYearly,
          label: t("_WordInsurance"),
          fill: true,
          backgroundColor: "pink",
          borderColor: "pink",
        },
        {
          data: balancesYearly,
          label: t("_LeftToPay"),
          fill: false,
          backgroundColor: "black",
          borderColor: "black",
        },
      ],
    },
    options: {
      responsive: true,
      plugins: {
        title: {
          display: true,
          text: "Chart.js Line Chart",
        },
        tooltip: {
          mode: "index",
          intersect: false,
        },
      },
      hover: {
        mode: "nearest",
        intersect: true,
      },
      scales: {
        x: {
          display: true,
          scaleLabel: {
            display: true,
            labelString: "Month",
          },
        },
        y: {
          display: true,
          scaleLabel: {
            display: true,
            labelString: "Value",
          },
        },
      },
    },
  };

  return (
    <div className="App">
      <CustomTypography
        value={t("GraphBankTitle")}
        variant={"h2"}
        typographyType="graphTitle"
      />
      <div>
        <Line data={config.data} options={config.options} />
      </div>
    </div>
  );
};

export default LineChart;

LineChart.propTypes = {
  graphData: PropTypes.object.isRequired,
};

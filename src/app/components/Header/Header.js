import React from "react";
import { NavLink, useHistory } from "react-router-dom";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import { useTranslation } from "react-i18next";

import { projects } from "../Route/RoutesConstant";
import { _Header, Button, Nav } from "./Header.styles";

const Header = () => {
  const history = useHistory();
  const { t } = useTranslation();

  const handleOnClickBack = () => {
    history.goBack();
  };

  const activeStyle = { color: "#F15B2A" };
  return (
    <_Header>
      <Button>
        <NavigateBeforeIcon onClick={handleOnClickBack} />
      </Button>
      <Nav>
        <NavLink to="/" activeStyle={activeStyle} exact>
          {t("HeaderHome")}
        </NavLink>
        {" | "}
        <NavLink to={projects} activeStyle={activeStyle}>
          {t("HeaderProjects")}
        </NavLink>
      </Nav>
    </_Header>
  );
};

export default Header;

import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import theme from "../Common/Style/Theme";

const StyledHeader = styled.header`
  margin: 1rem 6rem;
  display: flex;
  -webkit-box-pack: justify;
  justify-content: space-between;

  @media (max-width: ${theme.breakpoints.values.xs}px) {
    margin: 0.5rem 1.5rem;
  }

  @media (min-width: ${theme.breakpoints.values.xs}px) and (max-width: ${theme
      .breakpoints.values.sm}px) {
    margin: 0.5rem 2rem;
  }

  @media (min-width: ${theme.breakpoints.values.sm}px) and (max-width: ${theme
      .breakpoints.values.md}px) {
    margin: 1rem 4rem;
  }

  @media (min-width: ${theme.breakpoints.values.md}) {
    font-size: 18px;
    line-height: 24px;
  }
`;

export function _Header(props) {
  return <StyledHeader>{props.children}</StyledHeader>;
}

const StyledButton = styled.button`
  justify-self: flex-start;
  border: 0px;
  margin-left: -0.5rem;
  color: rgb(54, 47, 49);
  outline: none;
  background-color: transparent;
  padding: 0px;
  height: 2rem;
  width: 2rem;
`;

export function Button(props) {
  return <StyledButton>{props.children}</StyledButton>;
}

const StyledNav = styled.nav`
  justify-self: center;
  margin: auto;
  cursor: pointer;
`;

export function Nav(props) {
  return <StyledNav>{props.children}</StyledNav>;
}

_Header.propTypes = {
  children: PropTypes.node,
};

Button.propTypes = {
  children: PropTypes.node,
};

Nav.propTypes = {
  children: PropTypes.node,
};

export const bankCalculator = "/bank-calculator/:slug";
export const bankCalculatorId = "/bank-calculator/";

export const propertyCalculator = "/property-calculator/:slug";
export const propertyCalculatorId = "/property-calculator/";

export const project = "/projects/:slug";
export const projectId = "/projects/";

export const newproject = "/new-project";
export const introduction = newproject + "/introduction";
export const name = newproject + "/name";
export const purchasedate = newproject + "/purchase-date";
export const price = newproject + "/price";
export const newold = newproject + "/new-old";
export const notary = newproject + "/notary";
export const propertytype = newproject + "/property-type";
export const numberroom = newproject + "/number-room";
export const construction = newproject + "/construction";
export const furnitureCosts = newproject + "/furniture-costs";
export const area = newproject + "/area";
export const rent = newproject + "/rent";
export const contribution = newproject + "/contribution";
export const interestRate = newproject + "/interest-rate";
export const insuranceRate = newproject + "/insurance-rate";
export const loanDuration = newproject + "/loan-duration";

// Back end
export const projects = "/projects";
export const projectNew = "/projects/new";
export const projectUpdate = "/projects/update";
export const projectDelete = "/projects/delete/";

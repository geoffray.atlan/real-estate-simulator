import React from "react";
import { useTranslation } from "react-i18next";
import { CustomTypography } from "../Common/Style/Typography";

const PageNotFound = () => {
  const { t } = useTranslation();

  return <CustomTypography value={t("PageNotFoundContent")} variant={"h2"} />;
};

export default PageNotFound;

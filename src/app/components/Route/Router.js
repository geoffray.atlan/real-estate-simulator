/* eslint-disable import/no-named-as-default */
import React from "react";
import { Route, Switch } from "react-router-dom";
import HomePage from "../Home/HomePage";
import Collector from "../Collector";
import BankCalculator from "../Calculator/BankCalculator/BankCalculator";
import PropertyCalculator from "../Calculator/PropertyCalculator/PropertyCalculator";
import Projects from "../Projects";
import ManageProject from "../Projects/ManageProject/ManageProject";
import PageNotFound from "./PageNotFound";
import {
  bankCalculator,
  propertyCalculator,
  projects,
  project,
  newproject,
} from "./RoutesConstant";

export function Router() {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path={bankCalculator} component={BankCalculator} />
      <Route path={propertyCalculator} component={PropertyCalculator} />
      <Route path={project} component={ManageProject} />
      <Route path={projects} component={Projects} />
      <Route path={newproject} component={Collector} />
      <Route component={PageNotFound} />
    </Switch>
  );
}

export default Router;

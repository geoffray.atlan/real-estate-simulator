import React from "react";
import { _Footer, Div } from "./Footer.styles";
import ChangeLanguage from "../Common/Tools/ChangeLanguage";

const Footer = () => {
  return (
    <_Footer>
      <Div>
        <ChangeLanguage />
      </Div>
    </_Footer>
  );
};

export default Footer;

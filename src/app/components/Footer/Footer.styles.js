import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import theme from "../Common/Style/Theme";

const StyledFooter = styled.footer`
  background-color: rgb(245, 245, 245);
`;

export function _Footer(props) {
  return <StyledFooter>{props.children}</StyledFooter>;
}

const StyledDiv = styled.div`
  @media (max-width: ${theme.breakpoints.values.xs}px) {
    margin: 0.5rem 1.5rem;
  }

  @media (min-width: ${theme.breakpoints.values.xs}px) and (max-width: ${theme
      .breakpoints.values.sm}px) {
    margin: 0.5rem 2rem;
  }

  @media (min-width: ${theme.breakpoints.values.sm}px) and (max-width: ${theme
      .breakpoints.values.md}px) {
    margin: 0.8rem 4rem;
  }
  
  @media (min-width: ${theme.breakpoints.values.md}px) {
    margin: 0.8rem 4rem;
  }
  max-width: 90rem;
  margin: 0px auto;
`;

export function Div(props) {
  return <StyledDiv>{props.children}</StyledDiv>;
}

_Footer.propTypes = {
  children: PropTypes.node,
};

Div.propTypes = {
  children: PropTypes.node,
};

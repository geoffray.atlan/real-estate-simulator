import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, List, ListItem, ListItemIcon } from "@material-ui/core";
import HomeIcon from "@material-ui/icons/Home";
import ApartmentIcon from "@material-ui/icons/Apartment";
import styled from "styled-components";
import theme from "../../../Common/Style/Theme";
import { APARTMENT, PERCENT, EURO } from "../../../Common/Data/Constants";
import { CustomTypography } from "../../../Common/Style/Typography";
import { useTranslation } from "react-i18next";
import { localeString } from "../../../Common/Tools/LocaleString";

export const StyledProjectsContainer = styled.div`
  @media (max-width: ${theme.breakpoints.values.xs}px) {
    padding: 1.1875rem 1.3125rem 1.1875rem 1.3125rem;
  }
  position: relative;
  display: flex;
  flex-direction: row;
  -webkit-box-pack: start;
  justify-content: space-evenly;
  align-items: center;

  font-size: 1rem;
  line-height: 1.5rem;
  border-radius: 0.5rem;
  padding: 1.1875rem 1.3125rem 1.1875rem 0;
  border: 0px;
  margin-bottom: 0.5rem !important;
  height: auto;
`;

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  hidedIcon: {
    color: "white",
  },
}));

const StyledList = styled(List)`
  border: 0px;
  border-radius: 0.5rem;
  font-size: 1rem;
  height: auto;
  line-height: 1.5rem;
  margin-bottom: 0.5rem !important;
  padding: 1.1875rem 1.3125rem;
  width: 100%;

  @media (max-width: ${theme.breakpoints.values.xs}px) {
    margin: 0.5rem 1.5rem;
  }

  @media (min-width: ${theme.breakpoints.values.xs}px) and (max-width: ${theme
      .breakpoints.values.sm}px) {
    margin: 0.5rem 2rem;
  }

  @media (min-width: ${theme.breakpoints.values.sm}px) and (max-width: ${theme
      .breakpoints.values.md}px) {
    margin: 0.8rem 4rem;
  }
`;

export function NestedListItemTitle() {
  const classes = useStyles();
  const { t } = useTranslation();
  const [width, setWidth] = useState(window.innerWidth);
  const [orientation, setOrientation] = useState(
    theme.breakpoints.values.sm < window.innerWidth ? "vertical" : "horizontal"
  );
  useEffect(() => {
    const handleResize = () => setWidth(window.innerWidth);
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  });

  useEffect(() => {
    setOrientation(
      theme.breakpoints.values.sm < width ? "horizontal" : "vertical"
    );
  }, [width]);

  return (
    <>
      {orientation === "horizontal" && (
        <ListItem>
          <Grid container item direction="row">
            <Grid container item direction="row" spacing={2}>
              <ListItemIcon>
                <ApartmentIcon className={classes.hidedIcon} />
              </ListItemIcon>

              <Grid xs={2} item>
                <CustomTypography
                  value={t("CollectorNameTitle")}
                  variant={"h6"}
                  typographyType="elementList"
                  gutterBottom={false}
                  align="center"
                />
              </Grid>
              <Grid xs={2} item>
                <CustomTypography
                  value={t("_WordTaxes")}
                  variant={"h6"}
                  typographyType="elementList"
                  gutterBottom={false}
                />
              </Grid>
              <Grid xs={2} item>
                <CustomTypography
                  value={t("IndicatorPropertyNetProfitability")}
                  variant={"h6"}
                  typographyType="elementList"
                  gutterBottom={false}
                />
              </Grid>
              <Grid xs={2} item>
                <CustomTypography
                  value={t("IndicatorPropertyGrossProfitability")}
                  variant={"h6"}
                  gutterBottom={false}
                  typographyType="elementList"
                />
              </Grid>
              <Grid xs={2} item>
                <CustomTypography
                  value={t("CollectorPriceTitle")}
                  variant={"h6"}
                  gutterBottom={false}
                  typographyType="elementList"
                />
              </Grid>
            </Grid>
          </Grid>
        </ListItem>
      )}
    </>
  );
}

export function NestedList(props) {
  const classes = useStyles();

  return (
    <StyledList
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
    >
      {props.children}
    </StyledList>
  );
}

export function NestedListItem({ project, onClick }) {
  const { t } = useTranslation();
  const [width, setWidth] = useState(window.innerWidth);
  const [orientation, setOrientation] = useState(
    theme.breakpoints.values.sm < window.innerWidth ? "vertical" : "horizontal"
  );
  useEffect(() => {
    const handleResize = () => setWidth(window.innerWidth);
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  });

  useEffect(() => {
    setOrientation(
      theme.breakpoints.values.sm < width ? "horizontal" : "vertical"
    );
  }, [width]);

  const handleClick = () => {
    onClick();
  };

  const taxeSentence = () => {
    let istaxe = project.data.property.howManyYearsBeforeTaxes > 0
    let sentenceTaxe;

    if(!istaxe){
      sentenceTaxe = t("_WordNoTaxes")
    } else if (project.data.property.howManyYearsBeforeTaxes === 1){
      sentenceTaxe = t("_WordFor") +
      " " + project.data.property.howManyYearsBeforeTaxes +
      " " + t("_WordYear")
    } else {
      sentenceTaxe = t("_WordFor") +
      " " + project.data.property.howManyYearsBeforeTaxes +
      " " + t("_WordYears")
    }

    return orientation === "vertical"
      ? t("_WordTaxes") + ": " + sentenceTaxe
      : sentenceTaxe;
  };

  const grossProfitabilitySentence = () => {
    let grossProfitability = localeString(
      project.data.property.grossProfitability,
      PERCENT
    );
    return orientation === "vertical"
      ? t("IndicatorPropertyGrossProfitability") + ": " + grossProfitability
      : grossProfitability;
  };

  const netProfitabilitySentence = () => {
    let netProfitability = localeString(
      project.data.property.netProfitability,
      PERCENT
    );
    return orientation === "vertical"
      ? t("IndicatorPropertyNetProfitability") + ": " + netProfitability
      : netProfitability;
  };

  const PriceSentence = () => {
    let price = localeString(project.price, EURO);
    return orientation === "vertical"
      ? t("CollectorPriceTitle") + ": " + price
      : price;
  };

  return (
    <>
      <ListItem button onClick={() => handleClick()}>
        {orientation === "horizontal" && (
          <ListItemIcon>
            {project.propertyType === APARTMENT ? (
              <ApartmentIcon />
            ) : (
              <HomeIcon />
            )}
          </ListItemIcon>
        )}
        <Grid container item>
          <Grid
            container
            item
            direction={orientation === "horizontal" ? "row" : "column"}
            spacing={2}
          >
            <Grid sm={2} xs={12} item>
              <CustomTypography
                value={project.name}
                variant={"h6"}
                typographyType="elementList"
                gutterBottom={false}
              />
            </Grid>
            <Grid sm={3} xs={12} item>
              <CustomTypography
                value={taxeSentence()}
                variant={"h6"}
                typographyType="elementList"
                gutterBottom={false}
              />
            </Grid>
            <Grid sm={2} xs={12} item>
              <CustomTypography
                value={netProfitabilitySentence()}
                variant={"h6"}
                typographyType="elementList"
                gutterBottom={false}
              />
            </Grid>
            <Grid sm={2} xs={12} item>
              <CustomTypography
                value={grossProfitabilitySentence()}
                variant={"h6"}
                gutterBottom={false}
                typographyType="elementList"
              />
            </Grid>
            <Grid sm={2} xs={12} item>
              <CustomTypography
                value={PriceSentence()}
                variant={"h6"}
                gutterBottom={false}
                typographyType="elementList"
              />
            </Grid>
          </Grid>
        </Grid>
      </ListItem>
    </>
  );
}

NestedList.propTypes = {
  children: PropTypes.node,
};

NestedListItem.propTypes = {
  children: PropTypes.node,
  project: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};

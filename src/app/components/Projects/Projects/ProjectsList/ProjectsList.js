import React, { Fragment } from "react";
import PropTypes from "prop-types";
import DeleteIcon from "@material-ui/icons/Delete";
// import EditIcon from "@material-ui/icons/Edit";
import { useHistory } from "react-router-dom";
import { projectId } from "../../../Route/RoutesConstant";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  nestedListItem: {
    "& .MuiListItem-gutters": {
      paddingLeft: "none",
    },
  },
});

import {
  NestedList,
  NestedListItem,
  StyledProjectsContainer,
  NestedListItemTitle,
} from "./ProjectsList.style";
import { Paper } from "@material-ui/core";

export function ProjectsList({ projects, onDelete }) {
  const classes = useStyles();
  const history = useHistory();
  const handleOnDelete = (id) => {
    onDelete(id);
  };

  const openProject = (index) => {
    history.push(projectId + index);
  };

  // const editProject = (index) => {
  //   history.push(projectId + index);
  // };

  return (
    <>
      <NestedListItemTitle />
      <NestedList>
        {projects && projects.length > 0
          ? projects.map((project, index) => {
              return (
                <Fragment key={index + name}>
                  <Paper>
                    <StyledProjectsContainer>
                      <NestedListItem
                        className={classes.nestedListItem}
                        project={project}
                        index={index}
                        onClick={() => {
                          openProject(index);
                        }}
                      />
                      {/* <EditIcon onClick={() => editProject(index)} /> */}
                      <DeleteIcon onClick={() => handleOnDelete(index)} />
                    </StyledProjectsContainer>
                  </Paper>
                </Fragment>
              );
            })
          : null}
      </NestedList>
    </>
  );
}

ProjectsList.propTypes = {
  projects: PropTypes.array.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default ProjectsList;

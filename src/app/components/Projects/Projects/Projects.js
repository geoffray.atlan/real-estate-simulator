/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { introduction } from "../../Route/RoutesConstant";

import { connect } from "react-redux";
import {
  deleteProject,
  getProjects,
} from "../../../redux/actions/projectsActions";
import { updateNewProjectType } from "../../../redux/actions/userInteractionActions";

import { TitleAndInput, Title } from "./Projects.style";
import ProjectsList from "../Projects/ProjectsList";
import Button from "../../Common/UserInputs/Button";
import { NEW_PROJECT, EXISTING_PROJECT } from "../../Common/Data/Constants";

export function Projects({
  projects,
  getProjects,
  updateNewProjectType,
  deleteProject,
}) {
  const history = useHistory();
  const { t } = useTranslation();
  const [bufferProjects, setBufferProjects] = useState([]);

  useEffect(() => {
    setBufferProjects(projects);
  }, [projects]);

  const createProject = (projectType) => {
    updateNewProjectType(projectType)
    history.push(introduction);
  };

  async function handleDeleteProject(index) {
    await deleteProject(projects[index]._id);
    await getProjects();
  }

  return (
    <>
      <TitleAndInput>
        <Title title={t("ProjectsButtonMyProjects")} />

        <ProjectsList
          projects={bufferProjects}
          onDelete={handleDeleteProject}
        />
        <Button
          content={t("ProjectsLinkCreateProject")}
          selected={false}
          onClick={() => {
            createProject(NEW_PROJECT);
          }}
        />
        <Button
          content={t("ProjectsLinkAddExistingProject")}
          selected={false}
          onClick={() => {
            createProject(EXISTING_PROJECT);
          }}
        />
      </TitleAndInput>
    </>
  );
}

Projects.propTypes = {
  projects: PropTypes.array.isRequired,
  deleteProject: PropTypes.func.isRequired,
  getProjects: PropTypes.func.isRequired,
  updateNewProjectType: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    projects: state.projects,
  };
}

const mapDispatchToProps = {
  deleteProject,
  getProjects,
  updateNewProjectType,
};

export default connect(mapStateToProps, mapDispatchToProps)(Projects);

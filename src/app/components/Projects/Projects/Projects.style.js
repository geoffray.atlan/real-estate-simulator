import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { CustomTypography } from "../../Common/Style/Typography";
import theme from "../../Common/Style/Theme";

const StyledTitleAndInput = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  -webkit-box-pack: start;
  justify-content: flex-start;
  min-height: 40rem;
  position: relative;
  margin: 1rem 6rem;

  @media (max-width: ${theme.breakpoints.values.xs}px) {
    margin: 0.5rem 1.5rem;
  }

  @media (min-width: ${theme.breakpoints.values.xs}px) and (max-width: ${theme
      .breakpoints.values.sm}px) {
    margin: 0.5rem 2rem;
  }

  @media (min-width: ${theme.breakpoints.values.sm}px) and (max-width: ${theme
      .breakpoints.values.md}px) {
    margin: 0.8rem 4rem;
  }
`;

export function TitleAndInput(props) {
  return <StyledTitleAndInput>{props.children}</StyledTitleAndInput>;
}

const StyledHeader = styled.header`
  margin: 3.75rem auto;
  margin-left: 0;
`;

export function Title({ title }) {
  return (
    <>
      <StyledHeader>
        <CustomTypography value={title} variant={"h2"} />
      </StyledHeader>
    </>
  );
}

TitleAndInput.propTypes = {
  children: PropTypes.node,
};

Title.propTypes = {
  title: PropTypes.string.isRequired,
};

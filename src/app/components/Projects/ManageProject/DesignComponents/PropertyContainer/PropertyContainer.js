/* eslint-disable import/no-named-as-default */
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

import { Grid, Paper } from "@material-ui/core";
import PropTypes from "prop-types";
import { propertyCalculatorId } from "../../../../Route/RoutesConstant";
import SpecificationsContainer from "./SpecificationsContainer";
import IndicatorsContainer from "./IndicatorsContainer";
import CostsContainer from "./CostsContainer";
import {
  PROPERTY_TYPE,
  NEWOLD,
  NUMBER_ROOM,
  AREA,
  PURCHASE_DATE,
  PRICE,
  NOTARY,
  FURNITURE_COSTS,
  CONSTRUCTION,
  AGENCY_FEES,
  BROKER,
  BANK_FEES,
} from "../../../../Common/Data/Constants";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
  },
}));

export default function PropertyContainer({
  propertyType,
  newOld,
  numberRoom,
  area,
  notary,
  furnitureCosts,
  construction,
  id,
  price,
  dataProperty,
  bankFees,
  agencyFees,
  broker,
  purchaseDate,
  mode,
  onChange,
}) {
  const history = useHistory();

  const {
    priceToMeter,
    grossProfitability,
    netProfitability,
    howManyYearsBeforeTaxes,
  } = dataProperty;
  const [_propertyType, set_propertyType] = useState(propertyType);
  const [propertyNewOld, setPropertyNewOld] = useState(newOld);
  const [propertyNumberRoom, setPropertyNumberRoom] = useState(numberRoom);
  const [propertyArea, setPropertyArea] = useState(area);
  const [propertyPurchaseDate, setPropertyPurchaseDate] = useState(
    purchaseDate
  );
  const [propertyNotary, setPropertyNotary] = useState(notary);
  const [propertyFurnitureCosts, setPropertyFurnitureCosts] = useState(
    furnitureCosts
  );
  const [propertyConstruction, setPropertyConstruction] = useState(
    construction
  );
  const [propertyPrice, setPropertyPrice] = useState(price);
  const [propertyAgencyFees, setPropertyAgencyFees] = useState(agencyFees);
  const [propertyBroker, setPropertyBroker] = useState(broker);
  const [propertyBankFees, setPropertyBankFees] = useState(bankFees);
  const [totalProperty, setTotalProperty] = useState(0);

  // To send data upward at each change
  useEffect(() => {
    onChange({
      broker: propertyBroker,
      bankFees: propertyBankFees,
      agencyFees: propertyAgencyFees,
      propertyType: _propertyType,
      newOld: propertyNewOld,
      numberRoom: propertyNumberRoom,
      area: propertyArea,
      notary: propertyNotary,
      furnitureCosts: propertyFurnitureCosts,
      construction: propertyConstruction,
      price: propertyPrice,
      purchaseDate: propertyPurchaseDate,
    });
    calculateTotalProperty();
  }, [
    _propertyType,
    propertyBroker,
    propertyBankFees,
    propertyAgencyFees,
    propertyNewOld,
    propertyNumberRoom,
    propertyArea,
    propertyNotary,
    propertyFurnitureCosts,
    propertyConstruction,
    propertyPrice,
    propertyPurchaseDate
  ]);

  useEffect(() => {
    set_propertyType(propertyType);
  }, [propertyType]);

  useEffect(() => {
    setPropertyNewOld(newOld);
  }, [newOld]);

  useEffect(() => {
    setPropertyNumberRoom(numberRoom);
  }, [numberRoom]);

  useEffect(() => {
    setPropertyArea(area);
  }, [area]);

  useEffect(() => {
    setPropertyPurchaseDate(purchaseDate);
  }, [purchaseDate]);

  useEffect(() => {
    setPropertyNotary(notary);
  }, [notary]);

  useEffect(() => {
    setPropertyFurnitureCosts(furnitureCosts);
  }, [furnitureCosts]);

  useEffect(() => {
    setPropertyConstruction(construction);
  }, [construction]);

  useEffect(() => {
    setPropertyPrice(price);
  }, [price]);

  useEffect(() => {
    setPropertyAgencyFees(agencyFees);
  }, [agencyFees]);

  useEffect(() => {
    setPropertyBroker(broker);
  }, [broker]);

  useEffect(() => {
    setPropertyBankFees(bankFees);
  }, [bankFees]);

  const handleSpecificationOnChange = (what, value) => {
    if (what === PROPERTY_TYPE) set_propertyType(value);
    if (what === NEWOLD) setPropertyNewOld(value);
    if (what === NUMBER_ROOM) setPropertyNumberRoom(value);
    if (what === AREA) setPropertyArea(value);
    if (what === PURCHASE_DATE) setPropertyArea(value);
  };

  const handleCostsOnChange = (what, value) => {
    if (what === PRICE) setPropertyPrice(value);
    if (what === NOTARY) setPropertyNotary(value);
    if (what === FURNITURE_COSTS) setPropertyFurnitureCosts(value);
    if (what === CONSTRUCTION) setPropertyConstruction(value);
    if (what === AGENCY_FEES) setPropertyAgencyFees(value);
    if (what === BROKER) setPropertyBroker(value);
    if (what === BANK_FEES) setPropertyBankFees(value);
  };

  const calculateTotalProperty = () => {
    setTotalProperty(
      propertyPrice +
        propertyNotary +
        propertyFurnitureCosts +
        propertyAgencyFees +
        propertyBankFees +
        propertyBroker +
        propertyConstruction
    );
  };

  const toTable = () => {
    history.push({ pathname: propertyCalculatorId + id });
  };

  const classes = useStyles();

  return (
    <>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid container spacing={1} durection="row" justify="center">
            <Grid
              item
              container
              direction="row"
              justify="space-between"
              spacing={4}
            >
              <Grid
                item
                container
                direction="column"
                sm={12}
                md={6}
                justify="space-between"
              >
                <CostsContainer
                  price={price}
                  notary={notary}
                  furnitureCosts={furnitureCosts}
                  construction={construction}
                  agencyFees={agencyFees}
                  broker={broker}
                  bankFees={bankFees}
                  totalProperty={totalProperty}
                  mode={mode}
                  onChange={handleCostsOnChange}
                />
              </Grid>
              <Grid
                item
                container
                direction="column"
                sm={12}
                md={6}
                justify="space-between"
              >
                <SpecificationsContainer
                  propertyType={propertyType}
                  newOld={newOld}
                  numberRoom={numberRoom}
                  area={area}
                  purchaseDate={purchaseDate}
                  mode={mode}
                  onChange={handleSpecificationOnChange}
                />
              </Grid>
            </Grid>

            {/* Right Part */}
            <Grid container sm={12} md={6} item direction="column">
              <IndicatorsContainer
                priceToMeter={priceToMeter}
                grossProfitability={grossProfitability}
                netProfitability={netProfitability}
                howManyYearsBeforeTaxes={howManyYearsBeforeTaxes}
                onClick={toTable}
              />
            </Grid>
          </Grid>
        </Paper>
      </div>
    </>
  );
}

PropertyContainer.propTypes = {
  price: PropTypes.number.isRequired,
  newOld: PropTypes.string.isRequired,
  notary: PropTypes.number.isRequired,
  construction: PropTypes.number.isRequired,
  furnitureCosts: PropTypes.number.isRequired,
  area: PropTypes.number.isRequired,
  purchaseDate: PropTypes.number.isRequired,
  numberRoom: PropTypes.number.isRequired,
  propertyType: PropTypes.string.isRequired,
  dataProperty: PropTypes.object.isRequired,
  agencyFees: PropTypes.number.isRequired,
  broker: PropTypes.number.isRequired,
  bankFees: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

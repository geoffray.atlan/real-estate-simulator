/* eslint-disable import/no-named-as-default */
import React from "react";
import PropTypes from "prop-types";
import { Grid, Typography } from "@material-ui/core";
import { PERCENT } from "../../../../Common/Data/Constants";

import {
  GridRowTitle,
  GridRowWithKeyValue,
} from "../ManageProject.style";
import { useTranslation } from "react-i18next";

export default function IndicatorsContainer({
  priceToMeter,
  grossProfitability,
  netProfitability,
  howManyYearsBeforeTaxes,
  onClick,
}) {
  const { t } = useTranslation();

  return (
    <>
      <GridRowTitle titleValue={t("PropertyIndicators")} />

      <GridRowWithKeyValue
        valueKey={t("IndicatorPropertyPriceToMeter")}
        valueValue={priceToMeter}
      />

      <GridRowWithKeyValue
        valueKey={t("IndicatorPropertyGrossProfitability")}
        valueValue={grossProfitability}
        prefix={PERCENT}
      />

      <GridRowWithKeyValue
        valueKey={t("IndicatorPropertyNetProfitability")}
        valueValue={netProfitability}
        prefix={PERCENT}
      />

      <GridRowWithKeyValue
        valueKey={t("PropertyNoTaxe")}
        valueValue={howManyYearsBeforeTaxes}
        prefix=""
        suffix={t("_WordYear")}
      />

      <Grid item container alignContent="center" justify="center">
        <Typography variant="subtitle1">
          <button onClick={onClick}>{t("PropertyLinkToBankCalculator")}</button>
        </Typography>
      </Grid>
    </>
  );
}

IndicatorsContainer.propTypes = {
  howManyYearsBeforeTaxes: PropTypes.number.isRequired,
  grossProfitability: PropTypes.number.isRequired,
  priceToMeter: PropTypes.number.isRequired,
  netProfitability: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};

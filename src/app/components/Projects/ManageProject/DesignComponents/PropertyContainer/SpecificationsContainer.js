/* eslint-disable import/no-named-as-default */
import React from "react";
import PropTypes from "prop-types";
import { Grid } from "@material-ui/core";

import {
  StyledPicklistChoice,
  StyledTypoChoice,
  StyledDoubleButtonChoice,
  GridRowTitle,
  StyledDateChoice,
} from "../ManageProject.style";
import { useTranslation } from "react-i18next";
import MinMax from "../../../../Common/Data/MinMax.json";
import {
  HOUSE,
  APARTMENT,
  NEW,
  OLD,
  PROPERTY_TYPE,
  NEWOLD,
  NUMBER_ROOM,
  AREA,
  PURCHASE_DATE,
} from "../../../../Common/Data/Constants";

export default function SpecificationsContainer({
  propertyType,
  newOld,
  numberRoom,
  area,
  purchaseDate,
  mode,
  onChange,
}) {
  const { t } = useTranslation();

  const handlePropertyTypeOnChange = (value) => {
    onChange(PROPERTY_TYPE, value);
  };

  const handleNewOldOnChange = (value) => {
    onChange(NEWOLD, value);
  };

  const handleNumberRoomOnChange = (value) => {
    onChange(NUMBER_ROOM, value);
  };

  const handleAreaOnChange = (value) => {
    onChange(AREA, value);
  };

  const handlePurchaseDateOnChange = (value) => {
    onChange(PURCHASE_DATE, value);
  };

  return (
    <>
      <GridRowTitle titleValue={t("PropertySpecifications")} />

      {/* Content Specification */}
      <Grid item container xs direction="column">
        <Grid item container xs direction="column">
          <StyledDoubleButtonChoice
            mode={mode}
            what={t("CollectorPropertyTypeTitle")}
            value={propertyType}
            options={{
              content: [
                t("CollectorPropertyTypeOption1"),
                t("CollectorPropertyTypeOption2"),
              ],
              values: [APARTMENT, HOUSE],
            }}
            onChange={handlePropertyTypeOnChange}
          />
          <StyledDoubleButtonChoice
            mode={mode}
            what={t("CollectorNewOldTitle")}
            value={newOld}
            options={{
              content: [
                t("CollectorNewOldOption1"),
                t("CollectorNewOldOption2"),
              ],
              values: [NEW, OLD],
            }}
            onChange={handleNewOldOnChange}
          />
          <StyledPicklistChoice
            mode={mode}
            what={t("CollectorNumberRoomTitle")}
            numberRoom={numberRoom}
            numberRoomOptions={MinMax.numberOfRoom.options}
            onChangeNumberRoom={handleNumberRoomOnChange}
          />
          <StyledTypoChoice
            mode={mode}
            what={t("CollectorAreaTitle")}
            suffix={t("_WordAreaUnit")}
            area={area}
            onChangeArea={handleAreaOnChange}
          />
          <StyledDateChoice
            mode={mode}
            what={t("CollectorPurchaseDateTitle")}
            purchaseDate={purchaseDate}
            onChangePurchaseDate={handlePurchaseDateOnChange}
          />
        </Grid>
      </Grid>
    </>
  );
}

SpecificationsContainer.propTypes = {
  propertyType: PropTypes.string.isRequired,
  area: PropTypes.number.isRequired,
  newOld: PropTypes.string.isRequired,
  numberRoom: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  purchaseDate: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

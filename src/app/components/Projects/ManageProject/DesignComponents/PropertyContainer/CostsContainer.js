/* eslint-disable import/no-named-as-default */
import React from "react";
import PropTypes from "prop-types";

import { GridRowTitle, GridRowWithKeyValue } from "../ManageProject.style";
import { useTranslation } from "react-i18next";
import {
  PRICE,
  NOTARY,
  FURNITURE_COSTS,
  CONSTRUCTION,
  AGENCY_FEES,
  BROKER,
  BANK_FEES,
} from "../../../../Common/Data/Constants";

export default function CostsContainer({
  price,
  notary,
  furnitureCosts,
  construction,
  agencyFees,
  broker,
  bankFees,
  totalProperty,
  mode,
  onChange,
}) {
  const { t } = useTranslation();

  const handlePriceOnChange = (value) => {
    onChange(PRICE, value);
  };
  const handleNotaryOnChange = (value) => {
    onChange(NOTARY, value);
  };
  const handleFurnitureCostsOnChange = (value) => {
    onChange(FURNITURE_COSTS, value);
  };
  const handleConstructionOnChange = (value) => {
    onChange(CONSTRUCTION, value);
  };
  const handleAgencyFeesOnChange = (value) => {
    onChange(AGENCY_FEES, value);
  };
  const handleBrokerOnChange = (value) => {
    onChange(BROKER, value);
  };
  const handleBankFeesOnChange = (value) => {
    onChange(BANK_FEES, value);
  };

  return (
    <>
      <GridRowTitle titleValue={t("PropertyCosts")} />

      <GridRowWithKeyValue
        valueKey={t("CollectorPriceTitle")}
        valueValue={price}
        mode={mode}
        onChange={handlePriceOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorNotaryTitle")}
        valueValue={notary}
        mode={mode}
        onChange={handleNotaryOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorFurnitureCostsTitle")}
        valueValue={furnitureCosts}
        mode={mode}
        onChange={handleFurnitureCostsOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorConstructionTitle")}
        valueValue={construction}
        mode={mode}
        onChange={handleConstructionOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorAgencyFeesTitle")}
        valueValue={agencyFees}
        mode={mode}
        onChange={handleAgencyFeesOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorBrokerTitle")}
        valueValue={broker}
        mode={mode}
        onChange={handleBrokerOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorBankFeesTitle")}
        valueValue={bankFees}
        mode={mode}
        onChange={handleBankFeesOnChange}
      />

      <GridRowWithKeyValue
        valueKey={t("_WordTotal")}
        variant="subtitle1"
        valueValue={totalProperty}
      />
    </>
  );
}

CostsContainer.propTypes = {
  price: PropTypes.number.isRequired,
  notary: PropTypes.number.isRequired,
  furnitureCosts: PropTypes.number.isRequired,
  construction: PropTypes.number.isRequired,
  agencyFees: PropTypes.number.isRequired,
  broker: PropTypes.number.isRequired,
  bankFees: PropTypes.number.isRequired,
  totalProperty: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

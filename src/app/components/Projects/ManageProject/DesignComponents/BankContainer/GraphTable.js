/* eslint-disable import/no-named-as-default */
import React from "react";
import PropTypes from "prop-types";
import { Typography, Grid } from "@material-ui/core";


import { useTranslation } from "react-i18next";
import { CustomTypography } from "../../../../Common/Style/Typography";

export default function GraphTable({ onClick }) {
  const { t } = useTranslation();

  return (
    <>
      <Grid item container direction="row" xs={12} spacing={2}>
        <Grid item container direction="column" xs={4} alignContent="center">
          <CustomTypography
            value={t("PropertyLinkToBankCalculator")}
            variant={"subtitle1"}
          />
        </Grid>
        <Typography variant="subtitle1">
          <button onClick={onClick}>{t("BankLinkBankCalculator")}</button>
        </Typography>
      </Grid>
    </>
  );
}

GraphTable.propTypes = {
  onClick: PropTypes.func.isRequired,
};

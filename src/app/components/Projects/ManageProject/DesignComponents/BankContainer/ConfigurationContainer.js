/* eslint-disable import/no-named-as-default */
import React from "react";
import PropTypes from "prop-types";
import {
  PERCENT,
  CONTRIBUTION,
  INSURANCE_RATE,
  INTEREST_RATE,
  LOAN_DURATION,
} from "../../../../Common/Data/Constants";

import { GridRowTitle, GridRowWithKeyValue } from "../ManageProject.style";
import { useTranslation } from "react-i18next";

export default function ConfigurationContainer({
  contribution,
  insuranceRate,
  interestRate,
  loanDuration,
  loanAmount,
  mode,
  onChange,
}) {
  const { t } = useTranslation();

  const handleContributionOnChange = (value) => {
    onChange(CONTRIBUTION, value);
  };
  const handleInsuranceRateOnChange = (value) => {
    onChange(INSURANCE_RATE, value);
  };
  const handleInterestRateOnChange = (value) => {
    onChange(INTEREST_RATE, value);
  };
  const handleloanDurationOnChange = (value) => {
    onChange(LOAN_DURATION, value);
  };

  return (
    <>
      <GridRowTitle titleValue={t("PropertyConfiguration")} />

      <GridRowWithKeyValue
        valueKey={t("CollectorContributionTitle")}
        valueValue={contribution}
        mode={mode}
        onChange={handleContributionOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorInsuranceRateTitle")}
        valueValue={insuranceRate}
        mode={mode}
        prefix={PERCENT}
        onChange={handleInsuranceRateOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorInterestRateTitle")}
        valueValue={interestRate}
        mode={mode}
        prefix={PERCENT}
        onChange={handleInterestRateOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorloanDurationTitle")}
        valueValue={loanDuration}
        prefix={""}
        suffix={loanDuration > 1 ? t("_WordYears") : t("_WordYear")}
        mode={mode}
        onChange={handleloanDurationOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("BankLoanAmount")}
        valueValue={loanAmount}
        onChange={handleInterestRateOnChange}
      />
    </>
  );
}

ConfigurationContainer.propTypes = {
  contribution: PropTypes.number.isRequired,
  insuranceRate: PropTypes.number.isRequired,
  interestRate: PropTypes.number.isRequired,
  loanDuration: PropTypes.number.isRequired,
  loanAmount: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

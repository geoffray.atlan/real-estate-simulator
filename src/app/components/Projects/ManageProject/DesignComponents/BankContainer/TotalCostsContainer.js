/* eslint-disable import/no-named-as-default */
import React from "react";
import PropTypes from "prop-types";

import {
  GridRowTitle,
  GridRowWithKeyValue,
} from "../ManageProject.style";
import { useTranslation } from "react-i18next";

export default function TotalCostsContainer({
  equitiesTotal,
  insuranceTotal,
  interestsTotal,
  paymentMonthly,
  paymentTotal,
}) {
  const { t } = useTranslation();

  return (
    <>
      <GridRowTitle titleValue={t("PropertyTotalCosts")} />

      <GridRowWithKeyValue
        valueKey={t("CalculatorTotalPayments")}
        valueValue={paymentTotal}
      />
      <GridRowWithKeyValue
        valueKey={t("CalculatorTotalEquities")}
        valueValue={equitiesTotal}
      />
      <GridRowWithKeyValue
        valueKey={t("CalculatorTotalInterests")}
        valueValue={interestsTotal}
      />
      <GridRowWithKeyValue
        valueKey={t("CalculatorTotalInsurance")}
        valueValue={insuranceTotal}
      />
      <GridRowWithKeyValue
        valueKey={t("_WordMonthlyPayment")}
        valueValue={paymentMonthly}
      />
    </>
  );
}

TotalCostsContainer.propTypes = {
  equitiesTotal: PropTypes.number.isRequired,
  insuranceTotal: PropTypes.number.isRequired,
  interestsTotal: PropTypes.number.isRequired,
  paymentMonthly: PropTypes.number.isRequired,
  paymentTotal: PropTypes.number.isRequired,
};

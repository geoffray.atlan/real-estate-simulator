/* eslint-disable import/no-named-as-default */
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Paper } from "@material-ui/core";

import { bankCalculatorId } from "../../../../Route/RoutesConstant";
import ConfigurationContainer from "./ConfigurationContainer";
import TotalCostsContainer from "./TotalCostsContainer";
import GraphTable from "./GraphTable";

import {
  CONTRIBUTION,
  INSURANCE_RATE,
  INTEREST_RATE,
  LOAN_DURATION,
} from "../../../../Common/Data/Constants";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
  },
}));

export default function BankContainer({
  contribution,
  insuranceRate,
  interestRate,
  loanDuration,
  mode,
  dataBank,
  id,
  onChange,
}) {
  const history = useHistory();
  const {
    loanAmount,
    equitiesTotal,
    insuranceTotal,
    interestsTotal,
    paymentMonthly,
    paymentTotal,
  } = dataBank;

  const [bankContribution, setBankContribution] = useState(contribution);
  const [bankInsuranceRate, setBankInsuranceRate] = useState(insuranceRate);
  const [bankInterestRate, setBankInterestRate] = useState(interestRate);
  const [bankloanDuration, setBankloanDuration] = useState(loanDuration);

  // To send data upward at each change
  useEffect(() => {
    onChange({
      contribution: bankContribution,
      loanDuration: bankloanDuration,
      insuranceRate: bankInsuranceRate,
      interestRate: bankInterestRate,
    });
  }, [bankContribution, bankloanDuration, bankInsuranceRate, bankInterestRate]);

  useEffect(() => {
    setBankContribution(contribution);
  }, [contribution]);

  useEffect(() => {
    setBankInsuranceRate(insuranceRate);
  }, [insuranceRate]);

  useEffect(() => {
    setBankInterestRate(interestRate);
  }, [interestRate]);

  useEffect(() => {
    setBankloanDuration(loanDuration);
  }, [loanDuration]);

  const handleConfigurationOnChange = (what, value) => {
    if (what === CONTRIBUTION) setBankContribution(value);
    if (what === INSURANCE_RATE) setBankInsuranceRate(value);
    if (what === INTEREST_RATE) setBankInterestRate(value);
    if (what === LOAN_DURATION) setBankloanDuration(value);
  };

  const toCalculator = () => {
    history.push({ pathname: bankCalculatorId + id });
  };

  const classes = useStyles();

  return (
    <>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid
            item
            container
            direction="row"
            justify="space-between"
            spacing={4}
          >
            <Grid
              item
              container
              direction="row"
              sm={12}
              md={6}
              justify="space-between"
            >
              <ConfigurationContainer
                contribution={contribution}
                insuranceRate={insuranceRate}
                interestRate={interestRate}
                loanDuration={loanDuration}
                loanAmount={loanAmount}
                mode={mode}
                onChange={handleConfigurationOnChange}
              />
            </Grid>
            <Grid
              item
              container
              direction="row"
              sm={12}
              md={6}
              justify="space-between"
            >
              <TotalCostsContainer
                equitiesTotal={equitiesTotal}
                insuranceTotal={insuranceTotal}
                interestsTotal={interestsTotal}
                paymentMonthly={paymentMonthly}
                paymentTotal={paymentTotal}
              />
            </Grid>
          </Grid>
        </Paper>
      </div>
      <GraphTable onClick={toCalculator} />
    </>
  );
}

BankContainer.propTypes = {
  contribution: PropTypes.number.isRequired,
  insuranceRate: PropTypes.number.isRequired,
  interestRate: PropTypes.number.isRequired,
  loanDuration: PropTypes.number.isRequired,
  dataBank: PropTypes.object.isRequired,
  mode: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

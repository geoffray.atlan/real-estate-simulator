/* eslint-disable import/no-named-as-default */
import React from "react";
import PropTypes from "prop-types";
import { Grid } from "@material-ui/core";

import NumberInput from "../../../Common/UserInputs/NumberInput";
import TextInput from "../../../Common/UserInputs/TextInput";
import Picklist from "../../../Common/UserInputs/Picklist/Picklist";
import { CustomTypography } from "../../../Common/Style/Typography";
import Button from "../../../Common/UserInputs/Button";
import { EURO, EDIT, DISPLAY } from "../../../Common/Data/Constants";
import DatePicker from "../../../Common/UserInputs/DatePicker";

export function Title({ title, mode, variant, onChange }) {
  if (mode === DISPLAY)
    return (
      <CustomTypography
        value={title}
        variant={variant}
        typographyType={"titleProject"}
      />
    );
  else if (mode === EDIT) {
    return (
      <TextInput
        variant={variant}
        value={title}
        typographyType={"titleProject"}
        onChange={onChange}
      />
    );
  }
}

export function StyledDoubleButtonChoice({
  value,
  options,
  onChange,
  mode,
  what,
}) {
  let isDisabled = mode === DISPLAY ? true : false;

  const getTextValue = () => {
    return value === options.values[0]
      ? options.content[0]
      : options.content[1];
  };

  const handlOnChange = (value) => {
    value === options.content[0]
      ? onChange(options.values[0])
      : onChange(options.values[1]);
  };

  const selected = (buttonName) => {
    return buttonName === value ? true : false;
  };

  return (
    <>
      {isDisabled ? (
        <>
          <Grid item container direction="row">
            <Grid item container xs={6} direction="column">
              <CustomTypography value={what} variant={"subtitle2"} />
            </Grid>
            <Grid
              item
              container
              xs={6}
              direction="column"
              alignItems="flex-end"
            >
              <CustomTypography value={getTextValue()} variant={"subtitle2"} />
            </Grid>
          </Grid>
        </>
      ) : (
        <>
          <Grid item container direction="row" spacing={4}>
            <Grid item container direction="column" xs={4}>
              <CustomTypography value={what} variant={"subtitle2"} />
            </Grid>
            <Grid item container direction="column" xs={4}>
              <Button
                content={options.content[0]}
                selected={selected(options.values[0])}
                onClick={() => handlOnChange(options.content[0])}
              />
            </Grid>
            <Grid item container direction="column" justify="center" xs={4}>
              <Button
                content={options.content[1]}
                selected={selected(options.values[1])}
                onClick={() => handlOnChange(options.values[1])}
              />
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
}

export function StyledPicklistChoice({
  numberRoom,
  numberRoomOptions,
  what,
  onChangeNumberRoom,
  mode,
}) {
  let isDisabled = mode === DISPLAY ? true : false;
  return (
    <>
      <Grid item container direction="row">
        <Grid item container direction="column" xs={6} justify="flex-end">
          <CustomTypography value={what} variant={"subtitle2"} />
        </Grid>
        <Grid item container direction="column" xs={6} alignItems="flex-end">
          <Picklist
            name="Number of Rooms"
            value={numberRoom}
            onChange={onChangeNumberRoom}
            options={numberRoomOptions}
            disabled={isDisabled}
          />
        </Grid>
      </Grid>
    </>
  );
}

export function StyledTypoChoice({
  what,
  area,
  onChangeArea,
  mode,
  suffix,
  variant = "body2",
  color = "textPrimary",
}) {
  return (
    <>
      <Grid item container direction="row">
        <Grid item container direction="column" xs={6}>
          <CustomTypography value={what} variant={"subtitle2"} />
        </Grid>
        <Grid item container direction="column" xs={6} alignItems="flex-end">
          <NumberInput
            value={area}
            suffix={suffix}
            mode={mode}
            onChange={onChangeArea}
            variant={variant}
            color={color}
          />
        </Grid>
      </Grid>
    </>
  );
}

export function StyledDateChoice({
  what,
  purchaseDate,
  onChangePurchaseDate,
  mode,
}) {
  return (
    <>
      <Grid item container direction="row" justify="space-between">
        <Grid item container direction="column" xs={6} justify="flex-end">
          <CustomTypography value={what} variant={"subtitle2"} />
        </Grid>
        <Grid item container direction="column" xs={6} alignItems="flex-end">
          <DatePicker
            onChange={onChangePurchaseDate}
            value={new Date(purchaseDate)}
            disabled={mode === EDIT ? false : true}
          />
        </Grid>
      </Grid>
    </>
  );
}

export function GridRowTitle({ titleValue }) {
  return (
    <>
      <Grid item container xs direction="row" alignContent="center">
        <CustomTypography value={titleValue} variant={"subtitle1"} />
      </Grid>
    </>
  );
}

export function GridRowWithKeyValue({
  valueKey,
  valueValue,
  prefix = EURO,
  suffix = "",
  mode = DISPLAY,
  onChange,
  variant = "subtitle2",
}) {
  return (
    <>
      <Grid item container direction="row">
        <Grid item container xs direction="column">
          <CustomTypography
            value={valueKey}
            variant={variant}
            typographyType={"gridRowWithKeyValue"}
          />
        </Grid>
        <Grid item>
          {mode ? (
            <NumberInput
              prefix={prefix}
              suffix={suffix}
              value={valueValue}
              mode={mode}
              onChange={onChange}
              typographyType={"gridRowWithKeyValue"}
              variant={variant}
            />
          ) : (
            <NumberInput value={valueValue} variant={variant} />
          )}
        </Grid>
      </Grid>
    </>
  );
}

Title.propTypes = {
  title: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  mode: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired,
  align: PropTypes.string.isRequired,
};

StyledDoubleButtonChoice.propTypes = {
  value: PropTypes.string.isRequired,
  options: PropTypes.object.isRequired,
  mode: PropTypes.string.isRequired,
  what: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

StyledPicklistChoice.propTypes = {
  what: PropTypes.string.isRequired,
  numberRoom: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  onChangeNumberRoom: PropTypes.func.isRequired,
  numberRoomOptions: PropTypes.array.isRequired,
};

StyledTypoChoice.propTypes = {
  what: PropTypes.string.isRequired,
  area: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  suffix: PropTypes.string.isRequired,
  onChangeArea: PropTypes.func.isRequired,
  variant: PropTypes.string,
  color: PropTypes.string,
};

StyledDateChoice.propTypes = {
  what: PropTypes.string.isRequired,
  purchaseDate: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  onChangePurchaseDate: PropTypes.func.isRequired,
};

GridRowWithKeyValue.propTypes = {
  valueKey: PropTypes.string.isRequired,
  valueValue: PropTypes.number.isRequired,
  prefix: PropTypes.string,
  suffix: PropTypes.string,
  mode: PropTypes.string,
  onChange: PropTypes.func,
  variant: PropTypes.string,
  color: PropTypes.string,
};

GridRowTitle.propTypes = {
  titleValue: PropTypes.string.isRequired,
};

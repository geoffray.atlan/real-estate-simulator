/* eslint-disable import/no-named-as-default */
import React from "react";
import PropTypes from "prop-types";

import { GridRowWithKeyValue, GridRowTitle } from "../ManageProject.style";
import { useTranslation } from "react-i18next";
import {
  PROPERTY_TAX,
  OWNER_INSURANCE,
  COOWNERSHIP_CHARGES,
  ACCOUNTING,
} from "../../../../Common/Data/Constants";

export default function OpExContainer({
  accounting,
  coownershipCharges,
  monthlyPayment,
  propertyTax,
  ownerInsurance,
  mode,
  totalOpEx,
  onChange,
}) {
  const { t } = useTranslation();

  const handlePropertyTaxOnChange = (value) => {
    onChange(PROPERTY_TAX, value);
  };
  const handleOwnerInsuranceOnChange = (value) => {
    onChange(OWNER_INSURANCE, value);
  };
  const handleCoownershipChargesOnChange = (value) => {
    onChange(COOWNERSHIP_CHARGES, value);
  };
  const handleAccountingOnChange = (value) => {
    onChange(ACCOUNTING, value);
  };

  return (
    <>
      <GridRowTitle titleValue={t("OpExTitle")} />

      <GridRowWithKeyValue
        valueKey={t("_WordMonthlyPayment")}
        valueValue={monthlyPayment}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorPropertyTaxTitle")}
        valueValue={propertyTax}
        mode={mode}
        onChange={handlePropertyTaxOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorOwnerInsuranceTitle")}
        valueValue={ownerInsurance}
        mode={mode}
        onChange={handleOwnerInsuranceOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorAccountingTitle")}
        valueValue={accounting}
        mode={mode}
        onChange={handleAccountingOnChange}
      />
      <GridRowWithKeyValue
        valueKey={t("CollectorCoownershipChargesTitle")}
        valueValue={coownershipCharges}
        mode={mode}
        onChange={handleCoownershipChargesOnChange}
      />

      <GridRowWithKeyValue
        valueKey={t("_WordTotal")}
        variant="subtitle1"
        valueValue={totalOpEx}
      />
    </>
  );
}

OpExContainer.propTypes = {
  propertyTax: PropTypes.number.isRequired,
  ownerInsurance: PropTypes.number.isRequired,
  coownershipCharges: PropTypes.number.isRequired,
  accounting: PropTypes.number.isRequired,
  monthlyPayment: PropTypes.number.isRequired,
  totalOpEx: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

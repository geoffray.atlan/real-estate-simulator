/* eslint-disable import/no-named-as-default */
import React from "react";
import PropTypes from "prop-types";

import {
  GridRowWithKeyValue,
  GridRowTitle,
} from "../ManageProject.style";
import { useTranslation } from "react-i18next";

export function IncomeContainer({ rent, mode, totalIncome, onChange }) {
  const { t } = useTranslation();

  const handleRentOnChange = (newValue) => {
    onChange("rent", newValue);
  };

  return (
    <>
      <GridRowTitle titleValue={t("IncomeTitle")} />

      <GridRowWithKeyValue
        valueKey={t("CollectorRentTitle")}
        valueValue={rent}
        mode={mode}
        onChange={handleRentOnChange}
      />

      <GridRowWithKeyValue
        valueKey={t("_WordTotal")}
        variant="subtitle1"
        valueValue={totalIncome}
      />
    </>
  );
}

export default IncomeContainer;

IncomeContainer.propTypes = {
  rent: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  totalIncome: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

/* eslint-disable import/no-named-as-default */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Grid, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import OpExContainer from "./OpExContainer";
import IncomeContainer from "./IncomeContainer";
import {
  PROPERTY_TAX,
  OWNER_INSURANCE,
  COOWNERSHIP_CHARGES,
  ACCOUNTING,
} from "../../../../Common/Data/Constants";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
  },
}));

export default function PnLContainer({
  propertyTax,
  ownerInsurance,
  coownershipCharges,
  accounting,
  rent,
  monthlyPayment,
  mode,
  onChange,
}) {
  const classes = useStyles();

  const [bankpropertyTax, setBankpropertyTax] = useState(propertyTax);
  const [bankownerInsurance, setBankownerInsurance] = useState(ownerInsurance);
  const [bankcoownershipCharges, setBankcoownershipCharges] = useState(
    coownershipCharges
  );
  const [bankaccounting, setBankaccounting] = useState(accounting);
  const [bankrent, setBankrent] = useState(rent);
  const [totalOpEx, setTotalOpEx] = useState(
    propertyTax +
      ownerInsurance +
      accounting +
      coownershipCharges +
      monthlyPayment
  );
  const [totalIncome, setTotalIncome] = useState(rent);

  // To send data upward at each change
  useEffect(() => {
    onChange({
      accounting: bankaccounting,
      coownershipCharges: bankcoownershipCharges,
      propertyTax: bankpropertyTax,
      ownerInsurance: bankownerInsurance,
      rent: bankrent,
    });
    calculateTotal();
  }, [
    bankaccounting,
    bankcoownershipCharges,
    bankpropertyTax,
    bankownerInsurance,
    bankrent,
  ]);

  useEffect(() => {
    setBankpropertyTax(propertyTax);
  }, [propertyTax]);

  useEffect(() => {
    setBankownerInsurance(ownerInsurance);
  }, [ownerInsurance]);

  useEffect(() => {
    setBankcoownershipCharges(coownershipCharges);
  }, [coownershipCharges]);

  useEffect(() => {
    setBankaccounting(accounting);
  }, [accounting]);

  useEffect(() => {
    setBankrent(rent);
  }, [rent]);

  const handleOpExOnChange = (what, value) => {
    if (what === PROPERTY_TAX) setBankpropertyTax(value);
    if (what === OWNER_INSURANCE) setBankownerInsurance(value);
    if (what === COOWNERSHIP_CHARGES) setBankcoownershipCharges(value);
    if (what === ACCOUNTING) setBankaccounting(value);
  };

  const handleIncomeOnChange = (what, value) => {
    if (what === "rent") setBankrent(value);
  };

  const calculateTotal = () => {
    setTotalOpEx(
      bankpropertyTax +
        bankownerInsurance +
        bankaccounting +
        bankcoownershipCharges +
        monthlyPayment
    );
    setTotalIncome(bankrent);
  };

  return (
    <>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid
            item
            container
            direction="row"
            justify="space-between"
            spacing={4}
          >
            <Grid
              item
              container
              direction="row"
              sm={12}
              md={6}
              justify="space-between"
            >
              <IncomeContainer
                rent={rent}
                mode={mode}
                onChange={handleIncomeOnChange}
                totalIncome={totalIncome}
              />
            </Grid>
            <Grid
              item
              container
              direction="row"
              sm={12}
              md={6}
              justify="space-between"
            >
              <OpExContainer
                rent={rent}
                mode={mode}
                onChange={handleOpExOnChange}
                propertyTax={propertyTax}
                totalOpEx={totalOpEx}
                ownerInsurance={ownerInsurance}
                coownershipCharges={coownershipCharges}
                accounting={accounting}
                monthlyPayment={monthlyPayment}
              />
            </Grid>
          </Grid>
        </Paper>
      </div>
    </>
  );
}

PnLContainer.propTypes = {
  propertyTax: PropTypes.number.isRequired,
  ownerInsurance: PropTypes.number.isRequired,
  coownershipCharges: PropTypes.number.isRequired,
  accounting: PropTypes.number.isRequired,
  rent: PropTypes.number.isRequired,
  monthlyPayment: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { makeStyles } from "@material-ui/core/styles";
import { Tabs, Tab, Typography, Box, Grid } from "@material-ui/core";
import PhoneIcon from "@material-ui/icons/Phone";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import EuroIcon from "@material-ui/icons/Euro";

import { EDIT, DISPLAY } from "../../Common/Data/Constants";
import theme from "../../Common/Style/Theme";
import { Title } from "./DesignComponents/ManageProject.style";
import { CustomTypography } from "../../Common/Style/Typography";
import PropertyContainer from "./DesignComponents/PropertyContainer/PropertyContainer";
import PnLContainer from "./DesignComponents/PnLContainer/PnLContainer";
import BankContainer from "./DesignComponents/BankContainer/BankContainer";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography component="div">{children}</Typography>
        </Box>
      )}
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
  },
  tab: {},
  tabPanel: {
    width: "100%",
    "& .MuiBox-root": {
      padding: 0,
    },
    
  },
  tabs: (props) => {
    return {
      width: "100%",
      borderRight: `1px solid ${theme.palette.divider}`,
      alignItems: "center",
      "& .MuiTabs-flexContainer": {
        justifyContent: "center",
      },
      "& .MuiTabs-indicator": {
        backgroundColor: "white",
      },
      
      maxWidth: props.orientation === "vertical" ? "8rem" : "none",
    };
  },
}));

export function ManageProjectDisplay({ project, mode, id, onChange }) {
  const { t } = useTranslation();
  const classes = useStyles();
  const {
    price,
    newOld,
    notary,
    construction,
    propertyType,
    numberRoom,
    area,
    rent,
    propertyTax,
    ownerInsurance,
    coownershipCharges,
    accounting,
    contribution,
    insuranceRate,
    interestRate,
    loanDuration,
    agencyFees,
    broker,
    bankFees,
    purchaseDate,
    furnitureCosts,
    data,
  } = project;

  let modeType = mode ? EDIT : DISPLAY;
  const [projectValues, setProjectValues] = useState(project);
  const [value, setValue] = useState(0);
  const [width, setWidth] = useState(window.innerWidth);
  const [orientation, setOrientation] = useState(
    theme.breakpoints.values.sm < window.innerWidth ? "vertical" : "horizontal"
  );

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    setProjectValues(project);
  }, [project]);

  useEffect(() => {
    const handleResize = () => setWidth(window.innerWidth);
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  });

  useEffect(() => {
    setOrientation(
      theme.breakpoints.values.sm < width ? "horizontal" : "vertical"
    );
  }, [width]);

  const handleTitleOnChange = (event) => {
    const { value } = event.target;
    let result = {
      ...projectValues,
      name: value,
    };
    onChange(result);
  };

  const handlePropertyOnChange = (propertyValues) => {
    if (typeof propertyValues !== "undefined") {
      let result = {
        ...projectValues,
        price: propertyValues.price,
        newOld: propertyValues.newOld,
        notary: propertyValues.notary,
        propertyType: propertyValues.propertyType,
        numberRoom: propertyValues.numberRoom,
        construction: propertyValues.construction,
        furnitureCosts: propertyValues.furnitureCosts,
        area: propertyValues.area,
        agencyFees: propertyValues.agencyFees,
        broker: propertyValues.broker,
        bankFees: propertyValues.bankFees,
      };
      onChange(result);
    }
  };

  const handleBankOnChange = (bankValues) => {
    if (typeof bankValues !== "undefined") {
      let result = {
        ...projectValues,
        contribution: bankValues.contribution,
        loanDuration: bankValues.loanDuration,
        insuranceRate: bankValues.insuranceRate,
        interestRate: bankValues.interestRate,
      };
      onChange(result);
    }
  };

  const handlePnLOnChange = (PnlValues) => {
    if (typeof PnlValues !== "undefined") {
      let result = {
        ...projectValues,
        propertyTax: PnlValues.propertyTax,
        ownerInsurance: PnlValues.ownerInsurance,
        coownershipCharges: PnlValues.coownershipCharges,
        accounting: PnlValues.accounting,
        rent: PnlValues.rent,
      };
      onChange(result);
    }
  };

  return (
    <>
      <Title
        onChange={handleTitleOnChange}
        mode={modeType}
        title={project.name}
        variant="h2"
        align="center"
      />
      <div className={classes.root}>
        <Grid item container direction={orientation === "horizontal" ? "row" : "column"} justify="space-between">
          <Grid item container direction="row" sm={3} md={2}>
            <Tabs
              orientation={orientation === "horizontal" ? "vertical" : "horizontal"}
              variant="scrollable"
              onChange={handleChange}
              className={classes.tabs}
              value={value}
            >
              <Tab label={<CustomTypography value={t("PropertyTitle")} variant={"caption"} color="secondary"/>} icon={<PhoneIcon />} />
              <Tab label={<CustomTypography value={t("BankTitle")} variant={"caption"}/>} icon={<AccountBalanceIcon />} />
              <Tab label={<CustomTypography value={t("PnLTitle")} variant={"caption"}/>} icon={<EuroIcon />} />
            </Tabs>
          </Grid>
          <Grid item container direction="row" sm justify="space-between">
            <TabPanel value={value} index={0} className={classes.tabPanel}>
              <PropertyContainer
                price={price}
                newOld={newOld}
                notary={notary}
                construction={construction}
                propertyType={propertyType}
                numberRoom={numberRoom}
                agencyFees={agencyFees}
                area={area}
                furnitureCosts={furnitureCosts}
                dataProperty={data.property}
                broker={broker}
                bankFees={bankFees}
                purchaseDate={purchaseDate}
                mode={modeType}
                id={id}
                onChange={handlePropertyOnChange}
              />
            </TabPanel>
            <TabPanel value={value} index={1} className={classes.tabPanel}>
              <BankContainer
                contribution={contribution}
                insuranceRate={insuranceRate}
                interestRate={interestRate}
                loanDuration={loanDuration}
                dataBank={data.bank}
                mode={modeType}
                id={id}
                onChange={handleBankOnChange}
              />
            </TabPanel>
            <TabPanel value={value} index={2} className={classes.tabPanel}>
              <PnLContainer
                propertyTax={propertyTax}
                ownerInsurance={ownerInsurance}
                rent={rent}
                coownershipCharges={coownershipCharges}
                accounting={accounting}
                dataPnL={data.Pnl}
                monthlyPayment={data.bank.paymentMonthly}
                mode={modeType}
                onChange={handlePnLOnChange}
              />
            </TabPanel>
          </Grid>
        </Grid>
      </div>
    </>
  );
}

ManageProjectDisplay.propTypes = {
  project: PropTypes.object.isRequired,
  mode: PropTypes.bool.isRequired,
  id: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

export default ManageProjectDisplay;

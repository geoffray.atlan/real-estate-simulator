/* eslint-disable import/no-named-as-default */
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

import { connect } from "react-redux";
import { saveProject } from "../../../redux/actions/projectsActions";
import { projects } from "../../Route/RoutesConstant";
import { TitleAndInput } from "../Projects/Projects.style";

import ManageProjectDisplay from "./ManageProjectDisplay";

export function ManageProject({ project, saveProject, id }) {
  const history = useHistory();
  const { t } = useTranslation();
  const [edit, setEdit] = useState(false);
  const [projectData, setProjectData] = useState(project);

  useEffect(() => {
    project.redirectToProject ? history.push(projects) : null;
    setProjectData(project);
  }, []);

  useEffect(() => {
    setProjectData(project);
  }, [project]);

  const handleOnEdit = () => {
    setEdit(!edit);
  };

  const handleOnChange = (res) => {
    setProjectData(res);
  };

  async function handleOnSave() {
    await saveProject(projectData, "update");
    handleOnEdit();
  }

  return (
    <>
      <TitleAndInput>
        {!edit && <button onClick={handleOnEdit}>{t("ButtonEdit")}</button>}
        {edit && <button onClick={handleOnSave}>{t("ButtonSave")}</button>}

        <ManageProjectDisplay
          project={projectData ? projectData : null}
          mode={edit}
          id={id}
          onChange={handleOnChange}
        />
      </TitleAndInput>
    </>
  );
}

ManageProject.propTypes = {
  project: PropTypes.object.isRequired,
  saveProject: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
};

function mapStateToProps(state, ownProps) {
  let project;
  const slug = ownProps.match.params.slug;
  const projects = state.projects;

  if (!slug || projects.length <= slug || projects.length === 0) {
    project = { redirectToProject: true };
  } else {
    project = projects[Number(slug)];
  }
  return {
    id: Number(slug),
    project,
  };
}

const mapDispatchToProps = {
  saveProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageProject);

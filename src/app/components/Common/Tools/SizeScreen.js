import theme from "../Style/Theme";

export function isDesktop() {
  let maxWidthMobile = theme.breakpoints.values.sm;
  let isDesktopScren = maxWidthMobile < window.innerWidth;
  return isDesktopScren;
}

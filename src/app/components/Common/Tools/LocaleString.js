/* eslint-disable import/no-named-as-default */
import PropTypes from "prop-types";
import { EURO, PERCENT } from "../Data/Constants";

export function localeString(value, prefix) {
  let options = {};
  if (prefix === EURO) {
    options = {
      style: "currency",
      currency: "EUR",
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    };
  } else if (prefix === PERCENT) {
    options = {
      style: "percent",
      minimumFractionDigits: 0,
      maximumFractionDigits: 2,
    };
  } else if (prefix === "") {
    options = {
      minimumFractionDigits: 0,
      maximumFractionDigits: 2,
    };
  }
  return value.toLocaleString("fr-FR", options);
}

localeString.propTypes = {
  value: PropTypes.number.isRequired,
  prefix: PropTypes.string.isRequired,
};

import React from "react";
import i18n from "../../../i18n";
import { EN, FR } from "../Data/Constants";
import { Grid } from "@material-ui/core";
import styled from "styled-components";
import _Button from "@material-ui/core/Button";
import { CustomTypography } from "../../Common/Style/Typography";

const StyledButton = styled(_Button)`
  border: 0px;
  outline: 0px;
  margin: 0px;
  padding: 0px;
  background-color: transparent;
  font-family: NEXTBook-Regular, sans-serif;
  text-transform: none;
  color: rgb(92, 106, 115);
`;

const ChangeLanguage = () => {
  const ChangeLanguage = (language) => {
    return () => {
      i18n.changeLanguage(language);
    };
  };
  return (
    <>
      <Grid
        container
        xs={4}
        item
        direction="column"
        spacing={2}
        alignItems="flex-start"
      >
        <Grid container xs={6} item direction="row">
          <StyledButton
            selected={false}
            variant="contained"
            onClick={ChangeLanguage(EN)}
          >
            <CustomTypography
              value="EN"
              variant={"button"}
              typographyType="languageButton"
            />
          </StyledButton>
        </Grid>
        <Grid container xs={6} item direction="row">
          <StyledButton
            variant="contained"
            selected={false}
            onClick={ChangeLanguage(FR)}
          >
            <CustomTypography
              value="FR"
              variant={"button"}
              typographyType="languageButton"
            />
          </StyledButton>
        </Grid>
      </Grid>
    </>
  );
};

ChangeLanguage.propTypes = {};

export default ChangeLanguage;

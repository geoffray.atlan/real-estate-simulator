import { NEW } from "../Data/Constants";


export function NotaryCalculation(newOld, price) {
  if (newOld !== "" && price !== 0) {
    return(
      newOld === NEW ? (price * 2.5) / 100 : (price * 8) / 100
    );
  } else {
    return 0;
  }
}

import React, { useState } from "react";
import Slider from "@material-ui/core/Slider";
import styled from "styled-components";
import NumberFormat from "react-number-format";
import PropTypes from "prop-types";
import { SYMBOLE_EURO, PERCENT_SYMBOLE } from "../Data/Constants";

const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  -webkit-box-align: center;
  align-items: center;
`;

const StyledNumberFormat = styled(NumberFormat)`
  max-width: 23rem;
  margin-bottom: 2rem;
`;

const StyledSlider = styled(Slider)`_SymboleEuro
  width: 100%;
`;

export default function InputSlider({
  min,
  max,
  valueIs,
  errorMessage,
  prefix = SYMBOLE_EURO,
  onChange,
}) {
  const [value, setValue] = useState(valueIs !== 0 ? valueIs : 0);

  const handleSliderChange = (event, newValue) => {
    setValue(newValue);
    onChange(value);
  };

  const RoundFunc = (value) => {
    return Math.round((value + Number.EPSILON) * 10000) / 10000;
  };

  const handleInputChange = (values) => {
    const { value } = values;
    let newValue = returnPercentageValue(value);
    setValue(newValue);
    onChange(newValue);
  };

  const handleBlur = () => {
    if (value < min) {
      setValue(min);
    } else if (value > max) {
      setValue(max);
    }
  };

  const returnPercentageValue = (value) => {
    return prefix === PERCENT_SYMBOLE ? RoundFunc(Number(value) / 100) : RoundFunc(Number(value));
  };

  const displayPercentageValue = () => {
    return prefix === PERCENT_SYMBOLE ? RoundFunc(value * 100) : RoundFunc(value);
  };

  return (
    <>
      <StyledContainer>
        <StyledNumberFormat
          value={displayPercentageValue()}
          thousandSeparator={true}
          prefix={prefix + " "}
          onValueChange={handleInputChange}
          onBlur={handleBlur}
          min={min}
          max={max}
        />
        <StyledSlider
          value={typeof value === "number" ? value : 0}
          step={prefix === PERCENT_SYMBOLE ? 0.0001: 1}
          onChange={handleSliderChange}
          aria-labelledby="input-slider"
          min={min}
          max={max}
        />
      </StyledContainer>
      {errorMessage && <p>{errorMessage}</p>}
    </>
  );
}

InputSlider.propTypes = {
  valueIs: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  prefix: PropTypes.string,
  errorMessage: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

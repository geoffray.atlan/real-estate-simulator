import React, { useState } from "react";
import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import { localeString } from "../Tools/LocaleString";
import { EDIT, DISPLAY, PERCENT } from "../Data/Constants";
import { CustomTypography } from "../Style/Typography";

export function NumberInput({
  value,
  onChange,
  mode = DISPLAY,
  prefix = "",
  suffix = "",
  variant,
  typographyType,
}) {
  let suffixAppend = suffix ? " " + suffix : "";

  const [numberValue, setNumberValue] = useState(value);

  const handleNumberChange = (values) => {
    const { value } = values;
    let newValue = returnPercentageValue(value);
    mode === EDIT ? (setNumberValue(newValue), onChange(newValue)) : null;
  };

  const returnPercentageValue = (value) => {
    return prefix === PERCENT ? Number(value) / 100 : Number(value);
  };

  const displayPercentageValue = () => {
    return prefix === PERCENT ? numberValue * 100 : numberValue;
  };

  const handleBlur = () => {
    // if (value < min) {
    //   setValue(min);
    // } else if (value > max) {
    //   setValue(max);
    // }
  };

  return (
    <>
      {mode === DISPLAY ? (
        <CustomTypography
          value={localeString(value, prefix) + suffixAppend}
          gutterBottom={true}
          typographyType={typographyType}
          variant={variant}
        />
      ) : (
        <NumberFormat
          value={displayPercentageValue()}
          displayType={mode === EDIT ? "number" : "text"}
          thousandSeparator={"'"}
          decimalSeparator={","}
          onValueChange={handleNumberChange}
          onBlur={handleBlur}
          // min={min}
          // max={max}
        />
      )}
    </>
  );
}

NumberInput.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  mode: PropTypes.string,
  prefix: PropTypes.string,
  variant: PropTypes.string,
  typographyType: PropTypes.string,
  suffix: PropTypes.string,
};

export default NumberInput;

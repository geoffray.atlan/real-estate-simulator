import React from "react";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import styled from "styled-components";
import { isDesktop } from "../Tools/SizeScreen";

const StyledTextField = styled(TextField)`
  text-align: "center";
`;

export default function TextInput({ onChange, value, autofocus = false }) {
  const useStyles = makeStyles((theme) => ({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(1),
        fontWeight: 500,
      },
      textAlign: "center",
    },
  }));
  const classes = useStyles();

  let isDesktopScren = isDesktop();

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <StyledTextField
        required
        defaultValue={value}
        onChange={onChange}
        autoFocus={isDesktopScren ? autofocus : false}
      />
    </form>
  );
}

TextInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  autofocus: PropTypes.bool,
};

import React from "react";
import PropTypes from "prop-types";
import _Button from "@material-ui/core/Button";
import styled from "styled-components";
import { CustomTypography } from "../Style/Typography";

const StyledContent = styled.span``;

const StyledButton = styled(_Button)`
  border-radius: 0.5rem;
  width: 100%;
  padding: 1.1875rem 1.3125rem;
  border: 0px;
  margin-bottom: 0.5rem !important;
  height: auto;
  color: ${(props) =>
    props.selected
      ? "rgb(54, 47, 49)  !important"
      : "rgb(54, 47, 49)  !important"};
  background-color: ${(props) =>
    props.selected
      ? "rgb(234, 247, 245) !important"
      : "transparent  !important"};

  box-shadow: ${(props) =>
    props.selected
      ? "rgb(35, 164, 143) 0px 0px 0px 1px inset !important;"
      : "rgb(54, 47, 49) 0px 0px 0px 1px inset  !important"};
`;

function Container(props) {
  return <StyledContent>{props.children}</StyledContent>;
}

export function Button({ content, selected, onClick }) {
  return (
    <StyledButton
      selected={selected}
      name={content}
      variant="contained"
      onClick={() => onClick(content)}
    >
      <CustomTypography value={content} variant={"button"} />
    </StyledButton>
  );
}

Button.propTypes = {
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  selected: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

Container.propTypes = {
  children: PropTypes.node,
};

export default Button;

import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { CustomTypography } from "../Style/Typography";

const StyledButton = styled.button`
  border-radius: 0.5rem;
  width: 100%;
  padding: 0.75rem 1.375rem;
  font-family: NextBook-Regular, sans-serif;
  font-weight: normal;
  outline: none;
  height: auto;
  margin: 0px auto;
  display: block;
  max-width: 23rem;

  color: ${(props) =>
    props.isDisabled ? "rgb(210, 214, 217)" : "rgb(255, 255, 255)"};
  background-color: ${(props) =>
    props.isDisabled ? "rgb(245, 245, 245)" : "rgb(54, 47, 49)"};
`;

export function NextButton({ content, isDisabled, onClickNext }) {
  return (
    <StyledButton onClick={onClickNext} isDisabled={isDisabled}>
      <CustomTypography
        value={content}
        variant={"button"}
        typographyType="nextButton"
      />
    </StyledButton>
  );
}

NextButton.propTypes = {
  content: PropTypes.string.isRequired,
  isDisabled: PropTypes.bool.isRequired,
  onClickNext: PropTypes.func.isRequired,
};

export default NextButton;

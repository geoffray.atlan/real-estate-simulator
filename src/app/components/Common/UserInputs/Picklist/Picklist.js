import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { MenuItem, FormControl, Select } from "@material-ui/core";
import styled from "styled-components";

const StyledSelect = styled(Select)`
  .label {
    visibility: hidden;
  }
`;

export default function Picklist({
  name,
  value,
  onChange,
  options,
  disabled = false,
}) {
  const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    root: {},
  }));
  const classes = useStyles();

  const handleChange = (event) => {
    onChange(event.target.value);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <StyledSelect
          className={classes.root}
          defaultValue={value}
          value={value}
          onChange={handleChange}
          label={value !== "" ? value : ""}
          disabled={disabled}
        >
          {options.map((option) => (
            <MenuItem key={name + " " + option} value={option}>
              {option}
            </MenuItem>
          ))}
        </StyledSelect>
      </FormControl>
    </div>
  );
}

Picklist.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
  disabled: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

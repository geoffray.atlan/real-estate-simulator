import React from "react";
import PropTypes from "prop-types";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import theme from "./Theme";

export const typographyTheme = createMuiTheme({
  breakpoints: theme.breakpoints,
  typography: {
    h2: {
      fontWeight: 500,
      [theme.breakpoints.down("md")]: {
        fontSize: "1.75rem",
        lineHeight: "2.25rem",
      },
      [theme.breakpoints.up("md")]: {
        fontSize: "2rem",
        lineHeight: "2.5rem",
      },
      textAlign: "center",
    },
    h5: {
      // fontWeight: 500,
      // fontSize: "1rem",
      // lineHeight: "1.5rem",
      // textTransform: "none",
    },
    h6: {
      // fontWeight: 500,
      // fontSize: "1rem",
      // lineHeight: "1.5rem",
      // textTransform: "none",
      textAlign: "center",
    },
    subtitle1: {
      // fontWeight: 500,
      // fontSize: "1rem",
      // lineHeight: "1.5rem",
      // textTransform: "none",
    },
    subtitle2: {
      // fontWeight: 500,
      // fontSize: "1rem",
      // lineHeight: "1.5rem",
      // textTransform: "none",
    },
    body2: {
      // fontWeight: 500,
      // fontSize: "1rem",
      // lineHeight: "1.5rem",
      // textTransform: "none",
      textAlign: "justify",
    },
    button: {
      // fontWeight: 500,
      // fontSize: "1rem",
      // lineHeight: "1.5rem",
      textTransform: "none",
    },
  },

  // palette: {
  //   primary: {
  //     main: purple[500],
  //   },
  //   secondary: {
  //     main: green[500],
  //   },
  // },
});

export const useStyles = makeStyles((theme) => ({
  typography: {
    color: theme.palette.primary.main,
  },
  gridRowWithKeyValue: {
    color: theme.palette.primary.main,
  },
  titleProject: {},
  nextButton: {
    fontFamily: theme.palette.secondary.main,
  },
  languageButton: {
    fontSize: "0.875rem",
  },
  elementList: {
    fontSize: "0.875rem",
    color: theme.palette.primary.main,
  },
}));

export function CustomTypography({
  value,
  variant,
  gutterBottom = true,
  capitalize = false,
  typographyType = "typography",
  color = 'inherit'
}) {
  const props = {
    gutterBottom,
    capitalize,
  };
  const classes = useStyles(props);
  return (
    <ThemeProvider theme={typographyTheme}>
      <Typography
        className={classes[typographyType]}
        gutterBottom={gutterBottom}
        variant={variant}
        color={color}
      >
        {value}
      </Typography>
    </ThemeProvider>
  );
}

CustomTypography.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  variant: PropTypes.string,
  typographyType: PropTypes.string,
  align: PropTypes.string,
  color: PropTypes.string,
  gutterBottom: PropTypes.bool,
  capitalize: PropTypes.bool,
};

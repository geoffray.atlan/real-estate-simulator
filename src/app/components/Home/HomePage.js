/* eslint-disable import/no-named-as-default */
import React from "react";
import { useHistory } from "react-router-dom";
import { CustomTypography } from "../Common/Style/Typography";
import Box from "@material-ui/core/Box";
import { useTranslation } from "react-i18next";

import { projects } from "../Route/RoutesConstant";
import NextButton from "../Common/UserInputs/NextButton";
import {
  TitleAndInput,
  Title,
  Section,
  Div,
} from "../Collector/Page/Collector.style";

export function HomePage() {
  const history = useHistory();
  const { t } = useTranslation();
  const handleOnClickNext = () => {
    history.push(projects);
  };

  return (
    <>
      <TitleAndInput>
        <Title title={t("HomeTitle")} />

        <Section>
          <CustomTypography value={t("HomeContent1")} variant="body2" />
        </Section>
        <Section>
          <CustomTypography value={t("HomeContent2")} variant="body2" />
        </Section>
        <Section>
          <CustomTypography value={t("HomeContent3")} variant="body2" />
        </Section>
        <Section>
          <Box fontStyle="italic">
            <CustomTypography value={t("HomeContent4")} variant="body2" />
          </Box>
        </Section>
        <Section>
          <CustomTypography value={t("HomeContent5")} variant="body2" />
        </Section>
      </TitleAndInput>

      <Div>
        <NextButton
          isDisabled={false}
          content={t("HomeButtonAccessProject")}
          onClickNext={handleOnClickNext}
        />
      </Div>
    </>
  );
}

export default HomePage;

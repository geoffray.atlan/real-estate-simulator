import React, { useState } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";
import {
  Paper,
  Table,
  TableHead,
  TableContainer,
  TableCell,
  TableBody,
  TableRow,
} from "@material-ui/core";
import PropTypes from "prop-types";
import { localeString } from "../../Common/Tools/LocaleString";
import { EURO } from "../../Common/Data/Constants";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function DisplayTable1({ bankData }) {
  const {
    equitiesMonthly,
    equitiesYearly,
    equitiesTotal,
    paymentMonthly,
    paymentYearly,
    paymentTotal,
    insuranceMonthly,
    insuranceYearly,
    insuranceTotal,
    interestsMonthly,
    interestsYearly,
    interestsTotal,
    balancesMonthly,
    balancesYearly,
  } = bankData;
  const { t } = useTranslation();
  const [showDetails, setShowDetails] = useState(false);
  const [showYearly, setShowYearly] = useState(false);
  const classes = useStyles();

  const createYearlyDataSet = () => {
    let rows = [];
    for (let index = 0; index < balancesYearly.length; index++) {
      let row = {
        period: index + 1,
        payment: localeString(paymentYearly, EURO),
        equitie: localeString(equitiesYearly[index], EURO),
        insurance: localeString(insuranceYearly, EURO),
        interests: localeString(interestsYearly[index], EURO),
        balance: localeString(balancesYearly[index], EURO),
      };
      rows.push(row);
    }
    return rows;
  };

  const createMonthyDataSet = () => {
    let rows = [];
    for (let index = 0; index < balancesMonthly.length; index++) {
      let row = {
        period: index + 1,
        payment: localeString(paymentMonthly, EURO),
        equitie: localeString(equitiesMonthly[index], EURO),
        insurance: localeString(insuranceMonthly, EURO),
        interests: localeString(interestsMonthly[index], EURO),
        balance: localeString(balancesMonthly[index], EURO),
      };
      rows.push(row);
    }
    return rows;
  };

  let monthlyRows = createMonthyDataSet();
  let yearlyRows = createYearlyDataSet();

  return (
    <>
      {showDetails && (
        <button onClick={() => setShowDetails(!showDetails)}>
          {t("ButtonHide")}
        </button>
      )}
      {!showDetails && (
        <button onClick={() => setShowDetails(!showDetails)}>
          {t("ButtonShow")}
        </button>
      )}
      {showDetails && (
        <button onClick={() => setShowYearly(!showYearly)}>
          {showYearly ? t("_WordMonthly") : t("_WordYearly")}
        </button>
      )}
      {showDetails && (
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>
                  {showYearly ? t("_WordYear") : t("_WordMonth")}
                </StyledTableCell>
                <StyledTableCell align="right">
                  {showYearly ? t("_WordYearlyPayment") : t("_WordMonthlyPayment")}
                </StyledTableCell>
                <StyledTableCell align="right">
                  {t("_WordEquity")}
                </StyledTableCell>
                <StyledTableCell align="right">
                  {t("_WordInterest")}
                </StyledTableCell>
                <StyledTableCell align="right">
                  {t("_WordInsurance")}
                </StyledTableCell>
                <StyledTableCell align="right">
                  {t("_WordBalance")}
                </StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {showYearly
                ? yearlyRows.map((row) => (
                    <StyledTableRow key={row.period}>
                      <StyledTableCell component="th" scope="row">
                        {row.period}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.payment}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.equitie}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.interests}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.insurance}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.balance}
                      </StyledTableCell>
                    </StyledTableRow>
                  ))
                : monthlyRows.map((row) => (
                    <StyledTableRow key={row.period}>
                      <StyledTableCell component="th" scope="row">
                        {row.period}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.payment}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.equitie}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.interests}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.insurance}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        {row.balance}
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
            </TableBody>
            <TableHead>
              <TableRow>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell align="right">
                  {localeString(paymentTotal, EURO)}
                </StyledTableCell>
                <StyledTableCell align="right">
                  {localeString(equitiesTotal, EURO)}
                </StyledTableCell>
                <StyledTableCell align="right">
                  {localeString(interestsTotal, EURO)}
                </StyledTableCell>
                <StyledTableCell align="right">
                  {localeString(insuranceTotal, EURO)}
                </StyledTableCell>
                <StyledTableCell align="right">{0}</StyledTableCell>
              </TableRow>
            </TableHead>
          </Table>
        </TableContainer>
      )}
    </>
  );
}

DisplayTable1.propTypes = {
  bankData: PropTypes.object.isRequired,
};

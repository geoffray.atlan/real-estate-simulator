/* eslint-disable import/no-named-as-default */
import React from "react";
import PropTypes from "prop-types";
import { Grid } from "@material-ui/core";

import { connect } from "react-redux";

import DisplayTable from "./DisplayTable";
import LineChart from "../../Graph/LineChart";
import { TitleAndInput } from "../../Projects/Projects/Projects.style";

export function BankCalculator({ project }) {
  const { bank } = project.data;

  return (
    <>
      <TitleAndInput>
        <Grid>
          <Grid>
            <LineChart graphData={bank.graphData} />
          </Grid>
          <Grid>
            <DisplayTable bankData={bank} />
          </Grid>
        </Grid>
      </TitleAndInput>
    </>
  );
}

BankCalculator.propTypes = {
  project: PropTypes.object.isRequired,
};

function mapStateToProps(state, ownProps) {
  let project;
  const slug = ownProps.match.params.slug;
  const projects = state.projects;
  if (!slug || projects.length <= slug || projects.length === 0) {
    project = { redirectToProject: true };
  } else {
    project = projects[Number(slug)];
  }
  return {
    id: Number(slug),
    project,
  };
}

export default connect(mapStateToProps)(BankCalculator);

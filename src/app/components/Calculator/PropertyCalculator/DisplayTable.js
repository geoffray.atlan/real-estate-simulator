import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";
import {
  Paper,
  Table,
  TableHead,
  TableContainer,
  TableCell,
  TableBody,
  TableRow,
} from "@material-ui/core";
import PropTypes from "prop-types";
import { localeString } from "../../Common/Tools/LocaleString";
import { EURO } from "../../Common/Data/Constants";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function DisplayTable({ propertyData, bankData, period }) {
  const {
    futurYearlyRentsAmounts,
    depreciation,
    futurYearlyCostsAmounts,
    futurYearlyUnknownCostsAmounts,
    EBIT,
    creditDepreciation,
    futureTaxes,
  } = propertyData;
  const { t } = useTranslation();
  const { interestsYearly } = bankData;
  const classes = useStyles();

  const createDataSet = () => {
    let rows = [];
    for (let index = 0; index < period; index++) {
      let row = {
        period: index + 1,
        futurYearlyRentsAmounts: localeString(
          futurYearlyRentsAmounts[index],
          EURO
        ),
        depreciation: localeString(depreciation[index], EURO),
        interestsYearly: localeString(interestsYearly[index], EURO),
        futurYearlyCostsAmounts: localeString(
          futurYearlyCostsAmounts[index],
          EURO
        ),
        futurYearlyUnknownCostsAmounts: localeString(
          futurYearlyUnknownCostsAmounts[index],
          EURO
        ),
        EBIT: localeString(EBIT[index], EURO),
        creditDepreciation: localeString(creditDepreciation[index], EURO),
        futureTaxes: localeString(futureTaxes[index], EURO),
      };
      rows.push(row);
    }
    return rows;
  };
  let rows = createDataSet();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>{t("_WordYear")}</StyledTableCell>
            <StyledTableCell align="right">{t("IncomeTitle")}</StyledTableCell>
            <StyledTableCell align="right">
              {t("_WordDepreciation")}
            </StyledTableCell>
            <StyledTableCell align="right">
              {t("_WordInterest")}
            </StyledTableCell>
            <StyledTableCell align="right">
              {t("PropertyCosts")}
            </StyledTableCell>
            <StyledTableCell align="right">
              {t("_WordUnknownCosts")}
            </StyledTableCell>
            <StyledTableCell align="right">{t("_WordEBIT")}</StyledTableCell>
            <StyledTableCell align="right">
              {t("_WordCreditDepreciation")}
            </StyledTableCell>
            <StyledTableCell align="right">{t("_WordTaxes")}</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.period}>
              <StyledTableCell component="th" scope="row">
                {row.period}
              </StyledTableCell>
              <StyledTableCell align="right">
                {row.futurYearlyRentsAmounts}
              </StyledTableCell>
              <StyledTableCell align="right">
                {row.depreciation}
              </StyledTableCell>
              <StyledTableCell align="right">
                {row.interestsYearly}
              </StyledTableCell>
              <StyledTableCell align="right">
                {row.futurYearlyCostsAmounts}
              </StyledTableCell>
              <StyledTableCell align="right">
                {row.futurYearlyUnknownCostsAmounts}
              </StyledTableCell>
              <StyledTableCell align="right">{row.EBIT}</StyledTableCell>
              <StyledTableCell align="right">
                {row.creditDepreciation}
              </StyledTableCell>
              <StyledTableCell align="right">{row.futureTaxes}</StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

DisplayTable.propTypes = {
  propertyData: PropTypes.object.isRequired,
  bankData: PropTypes.object.isRequired,
  period: PropTypes.number.isRequired,
};

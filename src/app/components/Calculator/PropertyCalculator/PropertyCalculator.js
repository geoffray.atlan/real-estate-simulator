import React from "react";
import PropTypes from "prop-types";

import { connect } from "react-redux";

import DisplayTable from "./DisplayTable";

export const PropertyCalculator = ({ project }) => {
  const { property, bank } = project.data;
  return (
    <>
      <DisplayTable
        propertyData={property}
        bankData={bank}
        period={project.loanDuration}
      />
    </>
  );
};

PropertyCalculator.propTypes = {
  project: PropTypes.object.isRequired,
};

function mapStateToProps(state, ownProps) {
  let project;
  const slug = ownProps.match.params.slug;
  const projects = state.projects;
  if (!slug || projects.length <= slug || projects.length === 0) {
    project = { redirectToProject: true };
  } else {
    project = projects[Number(slug)];
  }
  return {
    id: Number(slug),
    project,
  };
}

export default connect(mapStateToProps)(PropertyCalculator);

import {
  FETCH_PROJECTS_SUCCESS,
  UPDATE_PROJECT_SUCCESS,
  CREATE_PROJECT_SUCCESS,
  DELETE_PROJECT_SUCCESS,
} from "../actions/actionTypes";

const initialState = [];

export default function projectsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PROJECTS_SUCCESS:
      return [...action.projects];
    case CREATE_PROJECT_SUCCESS:
      return [...state, { ...action.project }];
    case UPDATE_PROJECT_SUCCESS:
      return state.map((project) =>
        project._id === action.project._id ? action.project : project
      );
    case DELETE_PROJECT_SUCCESS:
      return state.filter((project) => project._id !== action.id);
    default:
      return state;
  }
}

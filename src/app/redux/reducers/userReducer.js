import { UPDATE_USER_SUCCESS } from "../actions/actionTypes";

const initialState = {
  projectViewing: 0
};


export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_USER_SUCCESS:
      return { ...action.user };
    default:
      return state;
  }
}

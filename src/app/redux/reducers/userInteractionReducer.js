import { UPDATE_COLLECTOR_PAGE, UPDATE_NEW_PROJECT_TYPE } from "../actions/actionTypes";

const initialState = {
  progress: 0,
  newProjectType: "",
};

export default function userInteractionReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_COLLECTOR_PAGE:
      return { ...state, progress: action.progress };
    case UPDATE_NEW_PROJECT_TYPE:
      return { ...state, newProjectType: action.newProjectType };
    default:
      return state;
  }
}

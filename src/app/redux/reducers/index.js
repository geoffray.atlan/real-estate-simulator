import { combineReducers } from "redux";
import user from "./userReducer";
import project from "./projectReducer";
import projects from "./projectsReducer";
import userInteraction from "./userInteractionReducer";

const rootReducer = combineReducers({
  user,
  project,
  projects,
  userInteraction,
});

export default rootReducer;

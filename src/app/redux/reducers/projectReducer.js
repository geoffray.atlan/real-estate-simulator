import {
  UPDATE_NAME_SUCCESS,
  UPDATE_NEW_OLD_SUCCESS,
  UPDATE_PRICE_SUCCESS,
  UPDATE_NOTARY_SUCCESS,
  UPDATE_CONSTRUCTION_SUCCESS,
  UPDATE_TYPE_PROPRIETY_SUCCESS,
  UPDATE_NUMBER_ROOM_SUCCESS,
  UPDATE_FURNITURE_COSTS_SUCCESS,
  UPDATE_AREA_SUCCESS,
  UPDATE_PURCHASE_DATE_SUCCESS,
  UPDATE_RENT_SUCCESS,
  UPDATE_CONTRIBUTION_SUCCESS,
  UPDATE_INTEREST_RATE_SUCCESS,
  UPDATE_INSURANCE_RATE_SUCCESS,
  UPDATE_LOAN_DURATION_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  accounting: 350,
  agencyFees: 5000,
  area: 15,
  bankFees: 400,
  broker: 200,
  coownershipCharges: 500,
  construction: 6400,
  contribution: 30000,
  furnitureCosts: 1000,
  insuranceRate: 0.0015,
  interestRate: 0.0125,
  loanDuration: 25,
  name: "Allo",
  newOld: "NEW",
  notary: 10240,
  numberRoom: 1,
  ownerInsurance: 100,
  price: 128000,
  propertyType: "HOUSE",
  propertyTax: 800,
  purchaseDate: 0,
  rent: 720,
  user: "",
  indicators: {
    property: {},
    bank: {},
    PnL: {},
  },
  data: {
    property: {},
    bank: {},
    PnL: {},
  },
};

export default function projectReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_NAME_SUCCESS:
      return { ...state, name: action.name };
    case UPDATE_NEW_OLD_SUCCESS:
      return { ...state, newOld: action.newOld };
    case UPDATE_PRICE_SUCCESS:
      return { ...state, price: action.price };
    case UPDATE_NOTARY_SUCCESS:
      return { ...state, notary: action.notary };
    case UPDATE_CONSTRUCTION_SUCCESS:
      return { ...state, construction: action.construction };
    case UPDATE_TYPE_PROPRIETY_SUCCESS:
      return { ...state, propertyType: action.propertyType };
    case UPDATE_NUMBER_ROOM_SUCCESS:
      return { ...state, numberRoom: action.numberRoom };
    case UPDATE_FURNITURE_COSTS_SUCCESS:
      return { ...state, furnitureCosts: action.furnitureCosts };
    case UPDATE_AREA_SUCCESS:
      return { ...state, area: action.area };
    case UPDATE_PURCHASE_DATE_SUCCESS:
      return { ...state, purchaseDate: action.purchaseDate };
    case UPDATE_RENT_SUCCESS:
      return { ...state, rent: action.rent };
    case UPDATE_CONTRIBUTION_SUCCESS:
      return { ...state, contribution: action.contribution };
    case UPDATE_INTEREST_RATE_SUCCESS:
      return { ...state, interestRate: action.interestRate };
    case UPDATE_INSURANCE_RATE_SUCCESS:
      return { ...state, insuranceRate: action.insuranceRate };
    case UPDATE_LOAN_DURATION_SUCCESS:
      return { ...state, loanDuration: action.loanDuration };
    default:
      return state;
  }
}

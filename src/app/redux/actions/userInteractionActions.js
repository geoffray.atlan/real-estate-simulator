import {
  namePagesNewProjectList,
  namePagesExistingProjectList,
} from "../../components/Collector/Routes/RoutingLogic";
import { UPDATE_COLLECTOR_PAGE, UPDATE_NEW_PROJECT_TYPE } from "./actionTypes";
import {
  NEW_PROJECT,
  EXISTING_PROJECT,
} from "../../components/Common/Data/Constants";

export function updateProgressSuccess(progress) {
  return { type: UPDATE_COLLECTOR_PAGE, progress };
}

export function updateNewProjectTypeSuccess(newProjectType) {
  return { type: UPDATE_NEW_PROJECT_TYPE, newProjectType };
}

export function updateProgress(pageName) {
  return function (dispatch, getState) {
    let currentNewProjectType = getState().userInteraction.newProjectType;
    let namePages = [];
    if (currentNewProjectType === NEW_PROJECT) {
      namePages = namePagesNewProjectList;
    } else if (currentNewProjectType === EXISTING_PROJECT) {
      namePages = namePagesExistingProjectList;
    }

    let totalNumberPage = namePages.length;

    let progressToReturn = 0;
    const getProgress = () => {
      let next = namePages.findIndex((page) => page === pageName);
      next += 1;
      next /= totalNumberPage
      next *= 100
      return next;
    };
    progressToReturn = getProgress();

    dispatch(updateProgressSuccess(progressToReturn));
  };
}

export function updateNewProjectType(newProjectType) {
  return function (dispatch) {
    dispatch(updateNewProjectTypeSuccess(newProjectType));
  };
}

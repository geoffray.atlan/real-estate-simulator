import { UPDATE_USER_SUCCESS } from "./actionTypes";

export function updateUserSuccess(user) {
  return { type: UPDATE_USER_SUCCESS, user };
}

export function saveUser(user) {
  //eslint-disable-next-line no-unused-vars
  return function (dispatch, getState) {
    dispatch(updateUserSuccess(user));
  };
}

import axios from "axios";
import {
  FETCH_PROJECTS_REQUEST,
  FETCH_PROJECTS_SUCCESS,
  FETCH_PROJECTS_FAILURE,
  UPDATE_PROJECT_REQUEST,
  UPDATE_PROJECT_SUCCESS,
  UPDATE_PROJECT_FAILURE,
  CREATE_PROJECT_REQUEST,
  CREATE_PROJECT_SUCCESS,
  CREATE_PROJECT_FAILURE,
  DELETE_PROJECT_REQUEST,
  DELETE_PROJECT_SUCCESS,
  DELETE_PROJECT_FAILURE,
} from "./actionTypes";

import { CREATE } from "../../components/Common/Data/Constants";

import {
  projects,
  projectNew,
  projectUpdate,
  projectDelete,
} from "../../components/Route/RoutesConstant";

const url =
  process.env.NODE_ENV === "production" ? `` : `http://localhost:8888`;

export function getProjects() {
  return (dispatch) => {
    dispatch(getProjectRequest());
    return axios
      .get(url + projects)
      .then(({ data }) => {
        dispatch(getProjectSuccess(data));
      })
      .catch((error) => {
        dispatch(getProjectfailure(error));
        console.log(error);
      });
  };
}

export function saveProject(project, what) {
  return function (dispatch) {
    if (what === CREATE) {
      dispatch(createProjectRequest());
      return axios
        .post(url + projectNew, project)
        .then((response) => {
          dispatch(createProjectSuccess(response.data));
        })
        .catch((error) => {
          dispatch(createProjectFailure(error));
          if (error.response) {
            // The server responded with a status code range of 2xx
            console.log(error.response.data);
          } else if (error.request) {
            // No response was received
            console.log(error.request);
          } else {
            // Error when setting up the request
            console.log("Error", error.message);
          }
          console.log(error.config);
        });
    } else if (what === "update") {
      dispatch(updateProjectRequest());

      return axios

        .post(url + projectUpdate, project)

        .then((response) => {
          dispatch(updateProjectSuccess(response.data));
        })

        .catch((error) => {
          dispatch(updateProjectFailure(error));
          if (error.response) {
            // The server responded with a status code range of 2xx
            console.log(error.response.data);
          } else if (error.request) {
            // No response was received
            console.log(error.request);
          } else {
            // Error when setting up the request
            console.log("Error", error.message);
          }
          console.log(error.config);
        });
    }
  };
}

export function deleteProject(_id) {
  return function (dispatch) {
    dispatch(deleteProjectRequest());
    return axios
      .post(url + projectDelete + `${_id}`)
      .then((response) => {
        response.data ? dispatch(deleteProjectSuccess(_id)) : null;
      })
      .catch((error) => {
        if (error.response) {
          // The server responded with a status code range of 2xx
          console.log(error.response.data);
          dispatch(deleteProjectFailure(error));
        } else if (error.request) {
          // No response was received
          console.log(error.request);
        } else {
          // Error when setting up the request
          console.log("Error", error.message);
        }
        console.log(error.config);
      });
  };
}

export function getProjectRequest() {
  return { type: FETCH_PROJECTS_REQUEST };
}

export function getProjectSuccess(projects) {
  return { type: FETCH_PROJECTS_SUCCESS, projects };
}

export function getProjectfailure(error) {
  return { type: FETCH_PROJECTS_FAILURE, payload: error };
}

export function updateProjectRequest() {
  return { type: UPDATE_PROJECT_REQUEST };
}

export function updateProjectSuccess(project) {
  return { type: UPDATE_PROJECT_SUCCESS, project };
}

export function updateProjectFailure(error) {
  return { type: UPDATE_PROJECT_FAILURE, payload: error };
}

export function createProjectRequest() {
  return { type: CREATE_PROJECT_REQUEST };
}

export function createProjectSuccess(project) {
  return { type: CREATE_PROJECT_SUCCESS, project };
}

export function createProjectFailure(error) {
  return { type: CREATE_PROJECT_FAILURE, payload: error };
}

export function deleteProjectRequest() {
  return { type: DELETE_PROJECT_REQUEST };
}

export function deleteProjectSuccess(project) {
  return { type: DELETE_PROJECT_SUCCESS, project };
}

export function deleteProjectFailure(error) {
  return { type: DELETE_PROJECT_FAILURE, payload: error };
}

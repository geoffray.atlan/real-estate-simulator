import {
  NAME,
  PRICE,
  NEWOLD,
  NOTARY,
  PROPERTY_TYPE,
  NUMBER_ROOM,
  CONSTRUCTION,
  FURNITURE_COSTS,
  AREA,
  PURCHASE_DATE,
  RENT,
  CONTRIBUTION,
  INSURANCE_RATE,
  INTEREST_RATE,
  LOAN_DURATION,
} from "../../components/Common/Data/Constants";

import {
  UPDATE_NAME_SUCCESS,
  UPDATE_NEW_OLD_SUCCESS,
  UPDATE_PRICE_SUCCESS,
  UPDATE_NOTARY_SUCCESS,
  UPDATE_TYPE_PROPRIETY_SUCCESS,
  UPDATE_NUMBER_ROOM_SUCCESS,
  UPDATE_CONSTRUCTION_SUCCESS,
  UPDATE_FURNITURE_COSTS_SUCCESS,
  UPDATE_AREA_SUCCESS,
  UPDATE_PURCHASE_DATE_SUCCESS,
  UPDATE_RENT_SUCCESS,
  UPDATE_CONTRIBUTION_SUCCESS,
  UPDATE_INTEREST_RATE_SUCCESS,
  UPDATE_INSURANCE_RATE_SUCCESS,
  UPDATE_LOAN_DURATION_SUCCESS,
} from "./actionTypes";

export function updateNameSuccess(name) {
  return { type: UPDATE_NAME_SUCCESS, name };
}
export function updatePriceSuccess(price) {
  return { type: UPDATE_PRICE_SUCCESS, price };
}
export function updateNewOldSuccess(newOld) {
  return { type: UPDATE_NEW_OLD_SUCCESS, newOld };
}
export function updateNotarySuccess(notary) {
  return { type: UPDATE_NOTARY_SUCCESS, notary };
}
export function updateConstructionSuccess(construction) {
  return { type: UPDATE_CONSTRUCTION_SUCCESS, construction };
}
export function updateTypeProprietySuccess(propertyType) {
  return { type: UPDATE_TYPE_PROPRIETY_SUCCESS, propertyType };
}
export function updateNumberRoomSuccess(numberRoom) {
  return { type: UPDATE_NUMBER_ROOM_SUCCESS, numberRoom };
}
export function updateFurnitureCostsSuccess(furnitureCosts) {
  return { type: UPDATE_FURNITURE_COSTS_SUCCESS, furnitureCosts };
}
export function updateAreaSuccess(area) {
  return { type: UPDATE_AREA_SUCCESS, area };
}
export function updatePurchaseDateSuccess(purchaseDate) {
  return { type: UPDATE_PURCHASE_DATE_SUCCESS, purchaseDate };
}
export function updateRentSuccess(rent) {
  return { type: UPDATE_RENT_SUCCESS, rent };
}

export function updateContributionSuccess(contribution) {
  return { type: UPDATE_CONTRIBUTION_SUCCESS, contribution };
}
export function updateInterestRateSuccess(interestRate) {
  return { type: UPDATE_INTEREST_RATE_SUCCESS, interestRate };
}
export function updateInsuranceRateSuccess(insuranceRate) {
  return { type: UPDATE_INSURANCE_RATE_SUCCESS, insuranceRate };
}
export function updateLoanDurationSuccess(loanDuration) {
  return { type: UPDATE_LOAN_DURATION_SUCCESS, loanDuration };
}

export function updateProject(element, value) {
  return function (dispatch) {
    switch (element) {
      case NAME:
        dispatch(updateNameSuccess(value));
        break;
      case PRICE:
        dispatch(updatePriceSuccess(value));
        break;
      case NEWOLD:
        dispatch(updateNewOldSuccess(value));
        break;
      case NOTARY:
        dispatch(updateNotarySuccess(value));
        break;
      case CONSTRUCTION:
        dispatch(updateConstructionSuccess(value));
        break;
      case PROPERTY_TYPE:
        dispatch(updateTypeProprietySuccess(value));
        break;
      case NUMBER_ROOM:
        dispatch(updateNumberRoomSuccess(value));
        break;
      case FURNITURE_COSTS:
        dispatch(updateFurnitureCostsSuccess(value));
        break;
      case AREA:
        dispatch(updateAreaSuccess(value));
        break;
      case PURCHASE_DATE:
        dispatch(updatePurchaseDateSuccess(value));
        break;
      case RENT:
        dispatch(updateRentSuccess(value));
        break;
      case CONTRIBUTION:
        dispatch(updateContributionSuccess(value));
        break;
      case INTEREST_RATE:
        dispatch(updateInterestRateSuccess(value));
        break;
      case INSURANCE_RATE:
        dispatch(updateInsuranceRateSuccess(value));
        break;
      case LOAN_DURATION:
        dispatch(updateLoanDurationSuccess(value));
        break;
      default:
        return;
    }
  };
}

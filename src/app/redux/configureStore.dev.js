import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reduxImmutableStateInvariant from "redux-immutable-state-invariant";
import rootReducer from "./reducers";
import { loadState } from "./sessionStorage";
// import { createLogger } from "redux-logger";

const initialState = loadState();
export default function configureStore() {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  // const logger = createLogger({
  //   collapsed: false,
  // });
  // const middlewares = [thunk, reduxImmutableStateInvariant(), logger];
  const middlewares = [thunk, reduxImmutableStateInvariant()]

  return createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(...middlewares))
  );
}

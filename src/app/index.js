/* eslint-disable import/no-named-as-default */
import React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import configureStore from "./redux/configureStore";
import { Provider as ReduxProvider } from "react-redux";
import App from "./App";
import { saveState } from "./redux/sessionStorage";
import ScrollToTop from "./components/Route/ScrollToTop";
import { createGlobalStyle } from "styled-components";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./components/Common/Style/Theme";

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
  }
  `;

export const store = configureStore();

// à reactiver pour remettre localStorage
store.subscribe(() => {
  saveState(store.getState());
});

render(
  <>
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <ReduxProvider store={store}>
        <Router>
          <ScrollToTop />
          <App />
        </Router>
      </ReduxProvider>
    </ThemeProvider>
  </>,
  document.getElementById("app")
);

/* eslint-disable import/default */
import path from "path";
import express from "express";
import cors from "cors";
import bodyParser from "body-parser";

import {Calculator} from './calculator/calculator';

import { get, getById, add, update, remove } from "./database/projects/projectsQueries";

let app = express();

// use CORS to accept requests from other domains
app.use(cors({
  origin: '*',
  credentials: true,
}));

app.use(bodyParser.urlencoded({ extended: true })); // use URL encoder to accept form posts
app.use(bodyParser.json()); // use JSON input parser

app.get("/projects", async (req, res) => {
  const ret = await get();
  res.json(ret);
});

app.post("/projects/new", async (req, res) => {
  let calculatorData = Calculator(req.body);
  const ret = await add(calculatorData);
  res.json(ret);
});

app.post("/projects/update", async (req, res) => {
  let calculatorData = Calculator(req.body);
  const ret = await update(req.body._id, calculatorData);
  res.json(ret);
});

app.get("/projects/:id", async (req, res) => {
  const ret = await getById(Number(req.params.id));
  res.json(ret);
});

app.post("/projects/delete/:id", async (req, res) => {
  const ret = await remove(req.params.id);
  res.json(ret);
});


if (process.env.NODE_ENV == `production`) {
  app.use(express.static(path.resolve('.')));
  app.get("/*", (req, res) => {
    res.sendFile(path.resolve("index.html"));
  });
}

// const port = process.env.NODE_ENV == `production` ? 8082 : 8888; // for production
const port =  8888; // for dev
var listener = app.listen(port, function () {
  console.log("Listening on port " + port);
});
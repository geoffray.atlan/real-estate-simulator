import { MongoClient, ObjectID } from "mongodb";
import assert from "assert";

  const username = "usertest";
  const password = "UserTest";
  const host = "@cluster0.dfsum.mongodb.net/";
  const dbName = "real-estate-simulator";
  const params =
    "?retryWrites=true&w=majority&useNewUrlParser=true&useUnifiedTopology=true";

  const url =
    "mongodb+srv://" + username + ":" + password + host + dbName + params;

  export async function get(query: any = {}, limit: number = 0) {
    const client = new MongoClient(url);
    try {
        await client.connect();
        const db = client.db(dbName);

        let items = db.collection("projects").find(query);
        // collection.find({}).project({ a: 1 })                             // Create a projection of field a
        // collection.find({}).skip(1).limit(10)                          // Skip 1 and limit 10
        // collection.find({}).batchSize(5)                               // Set batchSize on cursor to 5
        // collection.find({}).filter({ a: 1 })                              // Set query on the cursor
        // collection.find({}).comment('add a comment')                   // Add a comment to the query, allowing to correlate queries
        // collection.find({}).addCursorFlag('tailable', true)            // Set cursor as tailable
        // collection.find({}).addCursorFlag('oplogReplay', true)         // Set cursor as oplogReplay
        // collection.find({}).addCursorFlag('noCursorTimeout', true)     // Set cursor as noCursorTimeout
        // collection.find({}).addCursorFlag('awaitData', true)           // Set cursor as awaitData
        // collection.find({}).addCursorFlag('exhaust', true)             // Set cursor as exhaust
        // collection.find({}).addCursorFlag('partial', true)             // Set cursor as partial
        // collection.find({}).addQueryModifier('$orderby', { a: 1 })        // Set $orderby {a:1}
        // collection.find({}).max(10)                                    // Set the cursor max
        // collection.find({}).maxTimeMS(1000)                            // Set the cursor maxTimeMS
        // collection.find({}).min(100)                                   // Set the cursor min
        // collection.find({}).returnKey(10)                              // Set the cursor returnKey
        // collection.find({}).setReadPreference(ReadPreference.PRIMARY)  // Set the cursor readPreference
        // collection.find({}).showRecordId(true)                         // Set the cursor showRecordId
        // collection.find({}).sort([['a', 1]])                           // Sets the sort order of the cursor query
        // collection.find({}).hint('a_1')                                // Set the cursor hint
        if (limit > 0) {
          items = items.limit(limit);
        }
        return await items.toArray();
        
    } finally {
      client.close();
    }
  }

  export async function getById(id: number) {
    
    const client = new MongoClient(url);
    try {
      await client.connect();
      const db = client.db(dbName);
      const item = await db
      .collection("projects")
      .findOne({ id: id });
      // .findOne({ _id: new ObjectID(id) });
        return item;
      } finally {
        client.close();
      }
  }

  export async function add(item) {

    const client = new MongoClient(url);
      try {
        await client.connect();
        const db = client.db(dbName);
        const addedItem = await db.collection("projects").insertOne(item);

        return addedItem.ops[0];
      } finally {
        client.close();
      }
  }

  export async function update(id, newItem) {  
    delete newItem._id;
     
      const client = new MongoClient(url);
      try {
        await client.connect();
        const db = client.db(dbName);
        const updatedItem = await db
          .collection("projects")
          .findOneAndReplace({ _id: new ObjectID(id) }, newItem, {
            returnOriginal: false,
          });       

        return updatedItem.value;
      } finally {
        client.close();
      }
  }

  export async function remove(id) {
      const client = new MongoClient(url);
      try {
        await client.connect();
        const db = client.db(dbName);
        const removed = await db
          .collection("projects")
          .deleteOne({ _id: new ObjectID(id) });

        return removed.deletedCount === 1;
      } finally {
        client.close();
      }
  }

  export async function loadData(data) {
      const client = new MongoClient(url);
      try {
        await client.connect();
        const db = client.db(dbName);
        let results;
        for (let collectionName in data) {
          results = await db
            .collection(collectionName)
            .insertMany(data[collectionName]);
          assert.strictEqual(
            data[collectionName].length,
            results.insertedCount
          );
        }
        return results;
      } finally {
        client.close();
      }
  }

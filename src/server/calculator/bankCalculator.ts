import { LoanAmount, RoundFunc } from "./commonCalculation";

export default function BankCalculator(project) {
  let {
    contribution,
    loanDuration,
    notary,
    price,
    insuranceRate,
    interestRate,
    construction,
    agencyFees,
    furnitureCosts,
    bankFees,
    broker,
  } = project;

  const reducer = (accumulator, currentValue) => accumulator + currentValue;

  const _equitiesMonthly = () => {
    let bufferEquity = [];
    let equity = 0;
    for (let p = 0; p < numberOfMonths; p++) {
      let bufferValue = 0;
      // For each payment, figure out how much is interest
      let thisMonthsInterest = (loanAmount - equity) * interest;
      equity += paymentMonthly - thisMonthsInterest - insuranceMonthly; // The rest goes to equity

      p === 0
        ? (bufferValue = equity)
        : (bufferValue = equity - bufferEquity.reduce(reducer));
      bufferValue = RoundFunc(bufferValue);
      bufferEquity.push(bufferValue);

      if (p === numberOfMonths - 1) {
        equitiesTotal = RoundFunc(bufferEquity.reduce(reducer));
        return bufferEquity;
      }
    }
  };

  const _equitiesYearly = () => {
    let bufferArray = [];
    let yearValue = 0;

    for (let i = 1; i <= equitiesMonthly.length; i++) {
      yearValue += equitiesMonthly[i - 1];
      if ((i % 12 === 0 && i !== 1) || i === equitiesMonthly.length) {
        yearValue = RoundFunc(yearValue);
        bufferArray.push(yearValue);
        yearValue = 0;
      }
    }
    return bufferArray;
  };

  const _interestsMonthly = () => {
    let bufferInterest = [];
    let equity = 0;
    for (let p = 1; p <= numberOfMonths; p++) {
      // For each payment, figure out how much is interest
      let thisMonthsInterest = (loanAmount - equity) * interest;
      equity += paymentMonthly - thisMonthsInterest - insuranceMonthly; // The rest goes to equity
      thisMonthsInterest = RoundFunc(thisMonthsInterest);
      bufferInterest.push(thisMonthsInterest);

      if (p === numberOfMonths) {
        interestsTotal = RoundFunc(bufferInterest.reduce(reducer));
        return bufferInterest;
      }
    }
  };

  const _interestsYearly = () => {
    let bufferArray = [];
    let yearValue = 0;

    for (let i = 1; i <= interestsMonthly.length; i++) {
      yearValue += interestsMonthly[i - 1];
      if (i % 12 === 0 && i !== 1) {
        yearValue = RoundFunc(yearValue);
        bufferArray.push(yearValue);
        yearValue = 0;
      }
      if (i === interestsMonthly.length - 1) {
        yearValue = RoundFunc(yearValue);
        bufferArray.push(yearValue);
        yearValue = 0;
      }
    }
    return bufferArray;
  };

  const _balancesMonthly = () => {
    let bufferBalance = [];
    let bal = loanAmount;
    for (let p = 1; p <= numberOfMonths; p++) {
      // For each payment, figure out how much is interest
      let thisMonthsInterest = bal * interest;
      bal -= paymentMonthly - thisMonthsInterest - insuranceMonthly; // The rest goes to equity
      bal = RoundFunc(bal);
      bufferBalance.push(bal);

      if (p === numberOfMonths) {
        return bufferBalance;
      }
    }
  };

  const _balancesYearly = () => {
    let bufferArray = [];

    for (let i = 0; i < balancesMonthly.length; i++) {
      let yearValue = balancesMonthly[i];
      if (i % 12 === 0 && i !== 0) {
        yearValue = RoundFunc(yearValue);
        bufferArray.push(yearValue);
      }
      if (i === balancesMonthly.length - 1) {
        yearValue = RoundFunc(yearValue);
        bufferArray.push(yearValue);
      }
    }
    return bufferArray;
  };

  const _graphData = () => {
    let yearArray = [];
    let balancesYearlyArray = [];
    let equitiesYearlyArray = [];
    let interestsYearlyArray = [];
    let insuranceYearlyArray = [];
    yearArray.push(1);
    balancesYearlyArray.push(balancesYearly[0]);
    equitiesYearlyArray.push(equitiesYearly[0]);
    interestsYearlyArray.push(equitiesYearly[0] + interestsYearly[0]);
    insuranceYearlyArray.push(equitiesYearly[0] + interestsYearly[0] + insuranceYearly);

    for (let i = 1; i < equitiesYearly.length; i++) {
      yearArray.push(i + 1);
      balancesYearlyArray.push( RoundFunc(balancesYearly[i]));
      equitiesYearlyArray.push( RoundFunc(equitiesYearly[i] + equitiesYearlyArray[i - 1]));
      interestsYearlyArray.push( RoundFunc(equitiesYearly[i] + interestsYearly[i] + interestsYearlyArray[i - 1]));
      insuranceYearlyArray.push(
        RoundFunc(equitiesYearly[i] + interestsYearly[i] + insuranceYearly + insuranceYearlyArray[i - 1])
      );
    }
    return {
      year: yearArray,
      balancesYearly: balancesYearlyArray,
      equitiesYearly: equitiesYearlyArray,
      insuranceYearly: insuranceYearlyArray,
      interestsYearly: interestsYearlyArray,
    };
  };

  const loanAmount = LoanAmount(
    price,
    notary,
    construction,
    agencyFees,
    furnitureCosts,
    bankFees,
    broker,
    contribution
  );

  const numberOfMonths = loanDuration * 12;

  let insuranceMonthly = RoundFunc((loanAmount * insuranceRate * 100) / 1200);
  let insuranceYearly = RoundFunc(insuranceMonthly * 12);
  let insuranceTotal = RoundFunc(insuranceYearly * loanDuration);

  let interest = (interestRate * 100) / 100 / 12;

  let x = Math.pow(1 + interest, numberOfMonths);

  let paymentMonthly = RoundFunc(
    insuranceMonthly + (loanAmount * x * interest) / (x - 1)
  );
  let paymentYearly = RoundFunc(paymentMonthly * 12);
  let paymentTotal = RoundFunc(paymentYearly * loanDuration);

  let equitiesTotal;
  let equitiesMonthly = _equitiesMonthly();
  let equitiesYearly = _equitiesYearly();
  let interestsTotal;
  let interestsMonthly = _interestsMonthly();
  let interestsYearly = _interestsYearly();
  let balancesMonthly = _balancesMonthly();
  let balancesYearly = _balancesYearly();
  let graphData = _graphData();

  let dataTable = {
    loanAmount,
    balancesMonthly,
    balancesYearly,
    equitiesMonthly,
    equitiesYearly,
    equitiesTotal,
    insuranceMonthly,
    insuranceYearly,
    insuranceTotal,
    interestsMonthly,
    interestsYearly,
    interestsTotal,
    paymentMonthly,
    paymentYearly,
    paymentTotal,
    graphData,
  };

  return dataTable;
}

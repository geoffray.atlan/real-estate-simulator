import  BankCalculator  from "./bankCalculator";
import  PropertyCalculator  from "./propertyCalculator";

export function Calculator(project) {
  let projectInformed = project;

  let bankData = BankCalculator(project);
  projectInformed.data.bank = bankData;

  let propertyData = PropertyCalculator(project, projectInformed.data.bank.interestsYearly, projectInformed.data.bank.paymentMonthly);
  projectInformed.data.property = propertyData;
  
  return projectInformed;
}
import { RoundFunc } from "./commonCalculation";

export default function Depreciation(project) {
  let {
    loanDuration,
    notary,
    price,
    construction,
    agencyFees,
    furnitureCosts,
  } = project;

  const realEstataDuration = loanDuration;
  const realEstataPercent = 0.9;
  const realEstataValue = price + construction;
  const realEstataDepreciationTotal = RoundFunc(realEstataValue * realEstataPercent);
  const realEstataDepreciationYearly = RoundFunc(realEstataDepreciationTotal / realEstataDuration);
  const notaryDuration = loanDuration;
  const notaryPercent = 0.9;
  const notaryDepreciationTotal = RoundFunc(notary * notaryPercent);
  const notaryDepreciationYearly = RoundFunc(notaryDepreciationTotal / notaryDuration);
  const agencyFeesDuration = 10;
  const agencyFeesDepreciationYearly = RoundFunc(agencyFees / agencyFeesDuration);
  const constructionDuration = 10;
  const constructionDepreciationYearly = RoundFunc(construction / constructionDuration);
  const furnitureDuration = 10;
  const furnitureDepreciationYearly = RoundFunc(furnitureCosts / furnitureDuration);

  const totalDepreciation = () => {
    let yearlyDepreciations = [];
    
    for (let index = 0; index < loanDuration; index++) {
      let yearlyDepreciation = 0;
      if(index < agencyFeesDuration){
        yearlyDepreciation = realEstataDepreciationYearly + notaryDepreciationYearly + agencyFeesDepreciationYearly + constructionDepreciationYearly + furnitureDepreciationYearly
      } else {
        yearlyDepreciation = realEstataDepreciationYearly + notaryDepreciationYearly
      }
      yearlyDepreciations.push(RoundFunc(yearlyDepreciation))
    }
    return yearlyDepreciations
  }

  return totalDepreciation();
}

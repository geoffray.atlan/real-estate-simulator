import { LoanAmount, RoundFunc } from "./commonCalculation";
import Depreciation from "./depreciation";

export default function PropertyCalculator(
  project,
  interestsYearly,
  paymentMonthly
) {
  let {
    contribution,
    loanDuration,
    notary,
    price,
    construction,
    agencyFees,
    area,
    rent,
    furnitureCosts,
    bankFees,
    broker,
    accounting,
    coownershipCharges,
    ownerInsurance,
    propertyTax,
  } = project;

  const loanAmount = LoanAmount(
    price,
    notary,
    construction,
    agencyFees,
    furnitureCosts,
    bankFees,
    broker,
    contribution
  );

  let taxesRate = 0.3;
  let yearlyRent = rent * 12;
  let yearlyCost =
    accounting + coownershipCharges + +ownerInsurance + propertyTax;

  let priceToMeter = RoundFunc(price / area);
  let grossProfitability = Number(((rent * 12) / loanAmount).toFixed(4));

  let sumYearlyCost = Math.abs(yearlyCost + paymentMonthly * 12 - rent * 12);
  let netProfitability = Number((sumYearlyCost / loanAmount).toFixed(4));

  const calculateFuturRents = () => {
    let futurRents = [];
    futurRents.push(RoundFunc(yearlyRent));
    for (let index = 1; index < loanDuration; index++) {
      const element = futurRents[index - 1];
      futurRents.push(RoundFunc(element * 1.01)); //Raise the level of rent of 1% each year
    }
    return futurRents;
  };

  const calculateFuturCosts = () => {
    let futurCosts = [];
    futurCosts.push(RoundFunc(yearlyCost));
    for (let index = 1; index < loanDuration; index++) {
      const element = futurCosts[index - 1];
      futurCosts.push(RoundFunc(element * 1.01)); //Raise the level of rent of 1% each year
    }
    return futurCosts;
  };

  const calculateFuturUnknownCosts = () => {
    let futurUnknownCosts = [];
    for (let index = 0; index < loanDuration; index++) {
      index % 5 === 0 && index !== 0
        ? futurUnknownCosts.push(1000)
        : futurUnknownCosts.push(0); //Raise the level of rent of 1% each year
    }
    return futurUnknownCosts;
  };

  const calculateFuturEBITs = () => {
    let futurEBITs = [];
    for (let index = 0; index < loanDuration; index++) {
      const rent = futurYearlyRentsAmounts[index];
      const sumDepreciation = depreciation[index];
      const interests = interestsYearly[index];
      const costs = futurYearlyCostsAmounts[index];
      const unknownCosts = futurYearlyUnknownCostsAmounts[index];
      let element = rent - sumDepreciation - interests - costs - unknownCosts;
      futurEBITs.push(RoundFunc(element)); //Raise the level of rent of 1% each year
    }
    return futurEBITs;
  };

  const calculatecreditDepreciation = () => {
    let creditDepreciation = [];
    creditDepreciation.push(RoundFunc(EBIT[0]));
    for (let index = 1; index < loanDuration; index++) {
      const currentEBIT = EBIT[index];
      const lastCreditDepreciation = creditDepreciation[index - 1];
      currentEBIT + lastCreditDepreciation < 0
        ? creditDepreciation.push(
            RoundFunc(currentEBIT + lastCreditDepreciation)
          )
        : creditDepreciation.push(0);
    }
    return creditDepreciation;
  };

  const calculateFuturesTaxes = () => {
    let futureTaxes = [];
    for (let index = 0; index < loanDuration; index++) {
      creditDepreciation[index] < 0
        ? futureTaxes.push(0)
        : futureTaxes.push(
            RoundFunc(futurYearlyRentsAmounts[index] * taxesRate)
          );
    }
    return futureTaxes;
  };

  const getFirstYearTaxe = () => {
    for (let index = 0; index < futureTaxes.length; index++) {
      if (futureTaxes[index] !== 0) {
        return index;
      }
    }
    return 0;
  };

  let futurYearlyRentsAmounts = calculateFuturRents();
  let futurYearlyCostsAmounts = calculateFuturCosts();
  let futurYearlyUnknownCostsAmounts = calculateFuturUnknownCosts();
  let depreciation = Depreciation(project);
  let EBIT = calculateFuturEBITs(); // earnings before interest and taxes
  let creditDepreciation = calculatecreditDepreciation(); // earnings before interest and taxes
  let futureTaxes = calculateFuturesTaxes(); // earnings before interest and taxes
  let howManyYearsBeforeTaxes = getFirstYearTaxe();

  let dataTable = {
    priceToMeter,
    grossProfitability,
    netProfitability,
    futurYearlyRentsAmounts,
    futurYearlyCostsAmounts,
    futurYearlyUnknownCostsAmounts,
    depreciation,
    EBIT,
    creditDepreciation,
    futureTaxes,
    howManyYearsBeforeTaxes,
  };

  return dataTable;
}

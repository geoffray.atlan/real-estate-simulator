export const LoanAmount = (
  price,
  notary,
  construction,
  agencyFees,
  furnitureCosts,
  bankFees,
  broker,
  contribution
) => price + notary + construction + agencyFees + furnitureCosts + bankFees + broker - contribution;

export const RoundFunc = (value) => {
  return Math.round((value + Number.EPSILON) * 100) / 100;
};